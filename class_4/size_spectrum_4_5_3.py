"""
Sample Input:
4 -3 -2 3 3 -4 5 -3 -1 -1 3 4 1 3
1
8
Sample Output:
3
"""
#aa_masses = {"G": 57, "A": 71, "S": 87, "P": 97, "V": 99, "T": 101, "C": 103, "I": 113, "L": 113, "N": 114, "D": 115, "K": 128, "Q": 128, "E": 129, "M": 131, "H": 137, "F": 147, "R": 156, "Y": 163, "W": 186}	
#aa_masses = {57: "G", 71: "A", 87: "S", 97: "P", 99: "V", 101: "T", 103: "C", 113: "I", 114: "N", 115: "D", 128: "K", 129: "E", 131: "M", 137: "H", 147: "F", 156: "R", 163: "Y", 186: "W"}	
aa_masses = [128, 128, 97, 99, 131, 101, 71, 129, 137, 103, 113, 113, 114, 115, 147, 87, 57, 163, 156, 186]
#aa_masses = {4: "X", 5: "Z"}
#aa_masses = {"X": 4, "Z": 5}

input = "4 -3 -2 3 3 -4 5 -3 -1 -1 3 4 1 3"
threshold = 1
data_list = input.split(" ")
data = []
for each in data_list:
	data.append(int(each))
limit = 8

def size(data, threshold, limit):
	s_matrix = [[0 for i in range(limit+1)] for j in range(len(data)+1)]
	s_matrix[0][0] = 1
#	for each in s_matrix:
#		print each
	for i in range(1, len(data)+1):
		for j in range(1, limit+1):
			s_sum = 0
			for aa in aa_masses:
				new_i = i - aa
				new_j = j - data[i-1]
				if new_i >= 0 and new_j >= 0 and new_j <= limit:
					s_sum += s_matrix[new_i][new_j]
			s_matrix[i][j] = s_sum
#	for each in s_matrix:
#		print each
	return sum(s_matrix[-1][threshold:])
	
print size(data, threshold, limit)
