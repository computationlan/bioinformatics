"""
Sample Input:
4
0	20	17	11
20	0	20	13
17	20	0	10
11	13	10	0

Sample Output:
0->5:7.000
1->6:8.833
2->4:5.000
3->4:5.000
4->2:5.000
4->3:5.000
4->5:2.000
5->0:7.000
5->4:2.000
5->6:1.833
6->5:1.833
6->1:8.833
"""
raw_data = []
with open("dataset8.txt") as file:
	n_nodes = int(file.readline())
	while True:
		each = file.readline().rstrip() 		
		if len(each) == 0:
			break
		raw_data.append(each)
data_list = list()		
for each in raw_data:
	list1 = each.split(" ")
	list2 = list()
	for i in list1:
		list2.append(float(i))
	data_list.append(list2)		

def upgma(data_list, n_nodes):
	clusters = [n for n in range(n_nodes)]
	graph = {n:[] for n in range(n_nodes)}
	age = {n: 0 for n in range(n_nodes)}
	weight = {n: 1 for n in range(n_nodes)}
	new_node = n_nodes
	while len(clusters) > 1:
		min_c = float("inf")
		for i in range(len(data_list)):
			for j in range(len(data_list[i])):
				if i != j and data_list[i][j] < min_c:
					min_c = data_list[i][j]
					cluster1, cluster2 = i, j
		
		age[new_node] = min_c/2
		weight[new_node] = weight[clusters[cluster1]] + weight[clusters[cluster2]]
		length1 = age[new_node]-age[clusters[cluster1]]
		length2 = age[new_node]-age[clusters[cluster2]]
		graph[new_node] = [(clusters[cluster1], length1), (clusters[cluster2], length2)]
		graph[clusters[cluster1]].append((new_node, length1))
		graph[clusters[cluster2]].append((new_node, length2))
		
		new_row = []
		for i in range(len(data_list[0])):
			if i != cluster1 and i != cluster2:
				new_row.append((data_list[cluster1][i]*weight[clusters[cluster1]] + data_list[cluster2][i]*weight[clusters[cluster2]])/weight[new_node])
			elif i == cluster1:
				new_row.append(0)
		data_list.pop(cluster2)
		for i in range(len(data_list[0])-1):
			data_list[i][cluster1] = new_row[i]
			data_list[i].pop(cluster2) 
		data_list[cluster1] = new_row
		clusters[cluster1] = new_node
		clusters.pop(cluster2)
		new_node += 1
	return graph

result = upgma(data_list, n_nodes)
for each in result:
	for gr in result[each]:
		print '{}->{}:{:.3f}'.format(each, gr[0], gr[1])