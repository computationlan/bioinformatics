"""
Sample Input:
4
4->CAAATCCC
4->ATTGCGAC
5->CTGCGCTG
5->ATGGACGA
6->4
6->5

Sample Output:
16
ATTGCGAC->ATAGCCAC:2
ATAGACAA->ATAGCCAC:2
ATAGACAA->ATGGACTA:2
ATGGACGA->ATGGACTA:1
CTGCGCTG->ATGGACTA:4
ATGGACTA->CTGCGCTG:4
ATGGACTA->ATGGACGA:1
ATGGACTA->ATAGACAA:2
ATAGCCAC->CAAATCCC:5
ATAGCCAC->ATTGCGAC:2
ATAGCCAC->ATAGACAA:2
CAAATCCC->ATAGCCAC:5
"""

def small_parsimony(graph, labels, word_len):
	alph = "ACGT"
	leaves = [node for node in labels if node not in graph]

	for i in range(word_len):
		ll = {node: -1 for node in graph}
		for node in leaves:
			ll[node] = alph.index(labels[node][i])

		# initialize leaf scores
		scores = {node: [0,0,0,0] for node in graph}
		done = {node: False for node in graph}
		for node in leaves:
			inf = float("inf")
			scores[node] = [inf,inf,inf,inf]
			scores[node][ll[node]] = 0
			done[node] = True

		# count scores all the way up to the root
		while not all(done.values()):
			for node in graph:
				# check that all children have been processed
				if done[node]:
					continue
				ready = True
				for child in graph[node]:
					if not done[child]:
						ready = False
				if not ready:
					continue
				done[node] = True
				last_node = node
				# compute the letter scores
				for node_letter in range(4):
					for child in graph[node]:
						child_scores = list(scores[child])
						for letter in range(4):
							if letter != node_letter:
								child_scores[letter] += 1
						scores[node][node_letter] += min(child_scores)

		# pick best letters
		root = last_node
		ll[root] = scores[root].index(min(scores[root]))
		ready = set([root])
		while len(ready) > 0:
			node = ready.pop()
			node_letter = ll[node]
			for child in graph[node]:
				if child in leaves:
					continue
				child_scores = list(scores[child])
				for letter in range(4):
					if letter != node_letter:
						child_scores[letter] += 1
				ll[child] = child_scores.index(min(child_scores))
				ready.add(child)
				
		# combine letters into words
		for node in graph:
			labels[node] += alph[ll[node]]
			
	return labels

def dist(a, b):
	count = 0
	for each in range(len(a)):
		if a[each] != b[each]:
			count += 1
	return count

def print_parsimony(graph, labels):
	total = 0
	text = ''
	for node in graph:
		for child in graph[node]:
			d = dist(labels[node], labels[child])
			text += "{0}->{1}:{2}\n{1}->{0}:{2}\n".format(labels[node], labels[child], d)
			total += d
	print total
	print text
	
def read_parsimony(file):
	n_leaves = int(file.readline())
	raw_data = file.readlines()
	
	leaf = 0
	graph = {}
	labels = {}
	for line in raw_data[:n_leaves]:
		nodes = line.split("->")
		left = int(nodes[0])
		if left not in graph:
			graph[left] = list()
		graph[left].append(leaf)
		labels[leaf] = nodes[1].rstrip()
		leaf += 1
	for line in raw_data[n_leaves:]:
		nodes = line.split("->")
		left = int(nodes[0])
		right = int(nodes[1])
		if left not in graph:
			graph[left] = list()
		graph[left].append(right)
		labels[left] = ""
		labels[right] = ""
	return graph, labels


if __name__ == "__main__":
	with open("dataset10.txt") as file:
		graph, labels = read_parsimony(file)

	labels = small_parsimony(graph, labels, len(labels[0]))
	print_parsimony(graph, labels)
	