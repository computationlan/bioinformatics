"""
Sample Input:
4
0 23 27 20
23 0 30 28
27 30 0 30
20 28 30 0

Sample Output:
0->4:8.000
1->5:13.500
2->5:16.500
3->4:12.000
4->5:2.000
4->0:8.000
4->3:12.000
5->1:13.500
5->2:16.500
5->4:2.000
"""
raw_data = []
with open("dataset9.txt") as file:
	n_nodes = int(file.readline())
	while True:
		each = file.readline().rstrip() 		
		if len(each) == 0:
			break
		raw_data.append(each)
data_list = list()		
for each in raw_data:
	list1 = each.split(" ")
	list2 = list()
	for i in list1:
		list2.append(float(i))
	data_list.append(list2)		

intern_n = len(data_list)
nodes = [n for n in range(len(data_list))]

def join_neighbor(data_list, n_nodes):
	if n_nodes == 2:
		print "nodes", nodes, nodes[0], nodes[1]
		return {nodes[0]: [(nodes[1], data_list[0][1])], nodes[1]: [(nodes[0], data_list[0][1])]}
	else:
		total_dist = [sum(n) for n in data_list]
		matrix_a = [[0.0 for i in range(len(data_list[0]))]for j in range(len(data_list[0]))]
		for m in range(len(data_list[0])):
			for k in range(len(data_list[0])):
				if m != k:
					matrix_a[m][k] = (n_nodes - 2)*data_list[m][k] - total_dist[m] - total_dist[k]
		min_a = float("inf")
		for m in range(len(matrix_a[0])):
			for k in range(len(matrix_a[0])):
				if matrix_a[m][k] < min_a:
					min_a = matrix_a[m][k]
					index_1 = m
					index_2 = k
		
		delta = (total_dist[index_1] - total_dist[index_2])/(n_nodes-2)
		limb_1 = (data_list[index_1][index_2]+delta)/2.0
		limb_2 = (data_list[index_1][index_2]-delta)/2.0

		new_row = []
		for m in range(len(data_list[0])):
			if m != index_1 and m != index_2:
				new_row.append(0.5*(data_list[m][index_1] + data_list[m][index_2] - data_list[index_1][index_2]))
			elif m == index_1:
				new_row.append(0.0)
		data_list.pop(index_2)
		for i in range(len(data_list[0])-1):
			data_list[i][index_1] = new_row[i]
			data_list[i].pop(index_2) 
		data_list[index_1] = new_row
		old_1 = nodes[index_1]
		old_2 = nodes[index_2]
		global nodes
		nodes[index_1] = intern_n
		nodes.pop(index_2)
		print nodes
		global intern_n
		old_i = intern_n
		intern_n += 1
		graph = join_neighbor(data_list, len(data_list[0]))
		
		if old_1 in graph:
			graph[old_1].append((old_i, limb_1))
		else:
			graph[old_1] = [(old_i, limb_1)]
		if old_2 in graph:
			graph[old_2].append((old_i, limb_2))
		else:
			graph[old_2] = [(old_i, limb_2)]
		if old_i in graph:
			graph[old_i].append((old_1, limb_1))
			graph[old_i].append((old_2, limb_2))
		else:
			graph[old_i] = [(old_1, limb_1),(old_2, limb_2)]
	return graph
	
result = join_neighbor(data_list, n_nodes)
for each in result:
	for gr in result[each]:
		print str(each)+"->"+str(gr[0])+":"+str(gr[1])