"""
Sample Input:
57 71 154 185 301 332 415 429 486

Sample Output:
GPFNA
"""
aa_masses2 = {"G": 57, "A": 71, "S": 87, "P": 97, "V": 99, "T": 101, "C": 103, "I": 113, "L": 113, "N": 114, "D": 115, "K": 128, "Q": 128, "E": 129, "M": 131, "H": 137, "F": 147, "R": 156, "Y": 163, "W": 186}	
aa_masses = {57: "G", 71: "A", 87: "S", 97: "P", 99: "V", 101: "T", 103: "C", 113: "I", 114: "N", 115: "D", 128: "K", 129: "E", 131: "M", 137: "H", 147: "F", 156: "R", 163: "Y", 186: "W"}	

input = "0 57 71 154 185 301 332 415 429 486".split(" ")
data_list = []
for each in input:
	data_list.append(int(each))
	
def spectrum(data_list):
	result = []
	for mass in data_list:
		tick = mass
		for each in data_list:
			if each - tick in aa_masses:
				result.append((tick, each, aa_masses[each-tick]))

	return result
graph = spectrum(data_list)
	
def decoding_ideal_spectrum(data_list, graph):
	paths = [[0]]
	for each in graph:
		new_paths = list(paths)
		for path in paths:
			if each[0] == path[-1]:
				new_path = list(path)
				new_path.append(each[1])
				new_paths.append(new_path)
		paths = new_paths
	selection = []
	for each in paths:
		if each[-1] == data_list[-1]:
			selection.append(each)
	peptides = []
	for each in selection:
		peptide = ""
		ssum = 0
		for i in each[1:]:
			peptide += aa_masses[i - ssum]
			ssum = i 
		peptides.append(peptide)
	for each in peptides:
		spec = []
		mmas = 0 
		for i in each:
			spec.append(mmas+aa_masses2[i])
			mmas += aa_masses2[i]
		for j in each:
			spec.append(mmas-aa_masses2[j])
			mmas -= aa_masses2[j]
		if sorted(spec) == data_list:
			return each
	return 0
	
print decoding_ideal_spectrum(data_list, graph)	

			
