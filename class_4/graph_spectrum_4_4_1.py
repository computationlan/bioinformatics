"""
Sample Input:
57 71 154 185 301 332 415 429 486

Sample Output:
0->57:G
0->71:A
57->154:P
57->185:K
71->185:N
154->301:F
185->332:F
301->415:N
301->429:K
332->429:P
415->486:A
429->486:G
"""
#aa_masses = {"G": 57, "A": 71, "S": 87, "P": 97, "V": 99, "T": 101, "C": 103, "I": 113, "L": 113, "N": 114, "D": 115, "K": 128, "Q": 128, "E": 129, "M": 131, "H": 137, "F": 147, "R": 156, "Y": 163, "W": 186}	
aa_masses = {57: "G", 71: "A", 87: "S", 97: "P", 99: "V", 101: "T", 103: "C", 113: "I", 114: "N", 115: "D", 128: "K", 129: "E", 131: "M", 137: "H", 147: "F", 156: "R", 163: "Y", 186: "W"}	

input = "0 57 71 154 185 301 332 415 429 486".split(" ")
data_list = []
for each in input:
	data_list.append(int(each))
	
for mass in data_list:
	tick = mass
	for each in data_list:
		if each - tick in aa_masses:
			print str(tick)+"->"+str(each)+":"+aa_masses[each-tick]
			
