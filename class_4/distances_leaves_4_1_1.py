"""
Sample Input:
4
0->4:11
1->4:2
2->5:6
3->5:7
4->0:11
4->1:2
4->5:4
5->4:4
5->3:7
5->2:6
Sample Output:
0	13	21	22
13	0	12	13
21	12	0	13
22	13	13	0
"""

raw_data = []
with open("dataset1.txt") as file:
	n_nodes = int(file.readline())
	while True:
		each = file.readline().rstrip() 		
		if len(each) == 0:
			break
		raw_data.append(each)
data_dict = dict()		
for each in raw_data:
	out_node = int(each.split("->")[0])
	right_side = each.split("->")[1]
	in_node = right_side.split(":")[0]
	length = right_side.split(":")[1]
	if out_node not in data_dict:
		data_dict[out_node] = [(int(in_node), int(length))]
	else:
		data_dict[out_node].append((int(in_node), int(length)))
def scoring(data_dict, n_nodes):
	result_matrix = [[0 for i in range(n_nodes)] for j in range(n_nodes)]
	all_nodes = len(data_dict)
	for n in range(n_nodes):
		result_row = [0 for _ in range(all_nodes)]
		check_nodes = [n]   # check_tuples
		visited_nodes = []  # check_list
		while len(check_nodes) > 0:
			node = check_nodes.pop()
			score = result_row[node]
			visited_nodes.append(node)
			for each in data_dict[node]:
				if each[0] not in visited_nodes:
					check_nodes.append(each[0])
					result_row[each[0]] = score + each[1]
		result_matrix[n] = result_row[:n_nodes]
	return result_matrix

result_matrix = scoring(data_dict, n_nodes)	
for each in result_matrix:
	print " ".join(str(x) for x in each)