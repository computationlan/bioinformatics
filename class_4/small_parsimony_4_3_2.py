"""
Sample Input:
4
TCGGCCAA->4
4->TCGGCCAA
CCTGGCTG->4
4->CCTGGCTG
CACAGGAT->5
5->CACAGGAT
TGAGTACC->5
5->TGAGTACC
4->5
5->4

Sample Output:
17
TCGGCCAA->CCAGGCAC:4
CCTGGCTG->CCAGGCAC:3
TGAGTACC->CAAGGAAC:4
CCAGGCAC->CCTGGCTG:3
CCAGGCAC->CAAGGAAC:2
CCAGGCAC->TCGGCCAA:4
CACAGGAT->CAAGGAAC:4
CAAGGAAC->CACAGGAT:4
CAAGGAAC->TGAGTACC:4
CAAGGAAC->CCAGGCAC:2
"""
alph = "ACGT"
raw_data = []
with open("dataset11.txt") as file:
	n_nodes = int(file.readline())
	while True:
		each = file.readline().rstrip() 		
		if len(each) == 0:
			break
		raw_data.append(each)
leaves = dict()
nodes_dict = dict()		
for i in range(1, n_nodes*2, 2):
	out_node = int(raw_data[i].split("->")[0])
	right_side = raw_data[i].split("->")[1]
	if out_node not in leaves:
		leaves[out_node] = [right_side]
	else:
		leaves[out_node].append(right_side)
for i in range(n_nodes*2+1, len(raw_data)-1, 2):
	out_node = int(raw_data[i].split("->")[0])
	right_side = int(raw_data[i].split("->")[1])
	if out_node not in nodes_dict:
		nodes_dict[out_node] = [right_side]
	else:
		nodes_dict[out_node].append(right_side)
root = out_node + 1
nodes_dict[root] = [root - 1, root - 2]

word_len = len(leaves[n_nodes][0])

def dist(a, b):
	count = 0
	for each in range(len(a)):
		if a[each] != b[each]:
			count += 1
	return count

def small_parsimony(leaves, nodes_dict):
	words = {node: "" for node in range(n_nodes, root+1)}
	for i in range(word_len):
		j_dict = {}
		
		# from leaves to numbers
		for j in leaves:
			new_list = []
			for a in range(4):
				if alph[a] == leaves[j][0][i]:
					new_list.append(0)
				else:
					new_list.append(1)
				if alph[a] == leaves[j][1][i]:
					new_list[a] += 0
				else:
					new_list[a] += 1
			j_dict[j] = new_list
				
		# counts all the way up to the root
		for k in nodes_dict:
			new_list = []
			for a in range(4):
				ch = j_dict[nodes_dict[k][0]]
				number = float("inf")
				for n in range(len(ch)):
					if n != a and ch[n] + 1 < number:
						number = ch[n] + 1
					elif n == a and ch[n] < number:
						number = ch[n]
				ch1 = j_dict[nodes_dict[k][1]]
				number1 = float("inf")
				for m in range(len(ch)):
					if m != a and ch1[m] + 1 < number1:
						number1 = ch1[m] + 1
					elif m == a and ch1[m] < number1:
						number1 = ch1[m]
				new_list.append(number+number1)
			j_dict[k] = new_list
			
		# making words
		words[root] += alph[j_dict[root].index(min(j_dict[root]))]

		for node in range(root, n_nodes-1, -1):
			if not node in nodes_dict:
				continue
			for child in nodes_dict[node]:
				letter = words[node][-1]
				ch = j_dict[child]
				minimal = min(ch)
				if ch[alph.index(letter)] - 1 < minimal:
					words[child] += letter
				else:
					letter = alph[ch.index(minimal)]
					words[child] += letter
	
	# resulting graph
	graph = []
	nodes_dict.pop(root)
	for each in leaves:
		first = leaves[each][0]
		second = leaves[each][1]
		node = words[each]
		dist1 = dist(first, node)
		dist2 = dist(second, node)
		graph.append((first, node, dist1))
		graph.append((node, first, dist1))
		graph.append((second, node, dist2))
		graph.append((node, second, dist2))
	
	for each in nodes_dict:
		first = words[nodes_dict[each][0]]
		second = words[nodes_dict[each][1]]
		node = words[each]
		dist1 = dist(first, node)
		dist2 = dist(second, node)
		graph.append((first, node, dist1))
		graph.append((node, first, dist1))
		graph.append((second, node, dist2))
		graph.append((node, second, dist2))
		
	graph.append((words[root-1], words[root-2], dist(words[root-1], words[root-2])))
	graph.append((words[root-2], words[root-1], dist(words[root-1], words[root-2])))	
	return graph

result = small_parsimony(leaves, nodes_dict)
total = 0
for each in result:
	total += each[2]
	print each[0]+"->"+each[1]+":"+str(each[2])	
	
print total/2
	