"""
Sample Input:
-1 5 -4 5 3 -1 -4 5 -1 0 0 4 -1 0 1 4 4 4
-4 2 -2 -4 4 -5 -1 4 -1 2 5 -3 -1 3 2 -3
XXXZXZXXZXZXXXZXXZX
5
Sample Output:
XZXZ
"""
aa_masses = {"G": 57, "A": 71, "S": 87, "P": 97, "V": 99, "T": 101, "C": 103, "I": 113, "L": 113, "N": 114, "D": 115, "K": 128, "Q": 128, "E": 129, "M": 131, "H": 137, "F": 147, "R": 156, "Y": 163, "W": 186}	
#aa_masses = {57: "G", 71: "A", 87: "S", 97: "P", 99: "V", 101: "T", 103: "C", 113: "I", 114: "N", 115: "D", 128: "K", 129: "E", 131: "M", 137: "H", 147: "F", 156: "R", 163: "Y", 186: "W"}	
#aa_masses = {4: "X", 5: "Z"}
#aa_masses = {"X": 4, "Z": 5}

raw_data = []
with open("dataset14.txt") as file:
	while True:
		each = file.readline().rstrip() 		
		if len(each) == 0:
			break
		raw_data.append(each)
input_list = []
for each in raw_data:
	new_input = each.split(" ")
	new = [0]
	for i in new_input:
		new.append(int(i))
	input_list.append(new)

genome = "XXXZXZXXZXZXXXZXXZX"
threshold = 5
	
def problem(data, genome):
	score = -float("inf")
	best = ''
	max_len = len(genome) / min(aa_masses.values()) + 1
	for i in range(max_len):
		for k in range(len(genome)-i):
			new_score = 0
			pos = 0
			for m in range(i+1):
				pos += aa_masses[genome[k+m]]
				if pos < len(data):
					new_score += data[pos]
					last = pos
			if new_score > score and last == len(data)-1:
				score = new_score
				best = genome[k:k+i+1]
	return best, score
	
def psm(input_list, threshold, genome):
	psm_set = set()
	for each in input_list:
		peptide, score = problem(each, genome)
		if score >= threshold:
			psm_set.add(peptide)
	return psm_set
	
result = psm(input_list, threshold, genome)
for each in result:
	print each