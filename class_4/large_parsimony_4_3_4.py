"""
Sample Input:
5
GCAGGGTA->5
TTTACGCG->5
CGACCTGA->6
GATTCCAC->6
5->TTTACGCG
5->GCAGGGTA
5->7
TCCGTAGT->7
7->5
7->6
7->TCCGTAGT
6->GATTCCAC
6->CGACCTGA
6->7

Sample Output:
22
TCCGTAGT->TCAGCGGA:4
GATTCCAC->GAACCCGA:4
CGACCTGA->GAACCCGA:3
TTTACGCG->TCAGCGGA:5
TCAGCGGA->TTTACGCG:5
TCAGCGGA->GCAGCGGA:1
TCAGCGGA->TCCGTAGT:4
GCAGGGTA->GCAGCGGA:2
GCAGCGGA->GAACCCGA:3
GCAGCGGA->GCAGGGTA:2
GCAGCGGA->TCAGCGGA:1
GAACCCGA->GATTCCAC:4
GAACCCGA->CGACCTGA:3
GAACCCGA->GCAGCGGA:3

21
TCCGTAGT->TCTGCGGA:4
GATTCCAC->GCTGCGGA:5
CGACCTGA->GCAGCGGA:4
TTTACGCG->TCTGCGGA:4
TCTGCGGA->TTTACGCG:4
TCTGCGGA->GCTGCGGA:1
TCTGCGGA->TCCGTAGT:4
GCAGGGTA->GCAGCGGA:2
GCTGCGGA->GCAGCGGA:1
GCTGCGGA->GATTCCAC:5
GCTGCGGA->TCTGCGGA:1
GCAGCGGA->CGACCTGA:4
GCAGCGGA->GCAGGGTA:2
GCAGCGGA->GCTGCGGA:1
"""
from awesome_small_parsimony_4_3_1 import small_parsimony, dist
from awesome_small_parsimony_4_3_2 import read_unrooted_parsimony, print_parsimony, add_root
import copy
def nearest(node1, node2, data_dict):
	result = []
	leaves1 = [i for i in data_dict[node2] if i != node1]
	leaves2 = [i for i in data_dict[node1] if i != node2]	
	for j in range(2):
		inter_result = copy.deepcopy(data_dict)
		idx1 = inter_result[node2].index(leaves1[j])
		idx2 = inter_result[node1].index(leaves2[1])
		inter_result[node2][idx1] = leaves2[1]
		inter_result[node1][idx2] = leaves1[j]
		idx3 = inter_result[leaves1[j]].index(node2)
		idx4 = inter_result[leaves2[1]].index(node1)
		inter_result[leaves1[j]][idx3] = node1
		inter_result[leaves2[1]][idx4] = node2
		result.append(inter_result)
	
	return result
	
def score_f(graph, labels):
	total = 0
	for node in graph:
		for child in graph[node]:
			d = dist(labels[node], labels[child])
			total += d
	return total/2	

def solve_parsimony(graph, labels):
	labels2 = copy.deepcopy(labels)
	rooted_graph, labels2 = add_root(graph, labels2)
	labels2 = small_parsimony(rooted_graph, labels2, len(labels2[0]))
	score = score_f(graph, labels2)
	return score, labels2

with open("dataset13.txt") as file:
	graph, labels = read_unrooted_parsimony(file)

score = float("inf")
min_score, min_labels = solve_parsimony(graph, labels)
min_graph = graph
while min_score < score:
	print_parsimony(min_graph, min_labels)
	score = min_score
	graph = min_graph
	for each in graph:
		for k in graph[each]:
			if len(graph[each]) > 1 and len(graph[k]) > 1:
				new_graphs = nearest(each, k, graph)
				for m in new_graphs:
					new_score, new_labels = solve_parsimony(m, labels)
					if new_score < min_score:
						min_score = new_score
						min_graph = m
						min_labels = new_labels
