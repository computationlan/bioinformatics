"""
Sample Input:
0 0 0 1 0 0 0 0 1 0 0 0 0 1 0 0 0 1 0 0 0 1

Sample Output:
XZZXX
"""
#aa_masses = {"G": 57, "A": 71, "S": 87, "P": 97, "V": 99, "T": 101, "C": 103, "I": 113, "L": 113, "N": 114, "D": 115, "K": 128, "Q": 128, "E": 129, "M": 131, "H": 137, "F": 147, "R": 156, "Y": 163, "W": 186, "X": 4, "Z": 5}	
aa_masses = {57: "G", 71: "A", 87: "S", 97: "P", 99: "V", 101: "T", 103: "C", 113: "I", 114: "N", 115: "D", 128: "K", 129: "E", 131: "M", 137: "H", 147: "F", 156: "R", 163: "Y", 186: "W", 4: "X", 5: "Z"}	

input = "0 0 0 1 0 0 0 0 1 0 0 0 0 1 0 0 0 1 0 0 0 1"
data_list = input.split(" ")
data = []
for each in data_list:
	data.append(int(each))
def problem(data):
	result = ""
	count = 0
	for i in data:
		if i == 1:
			result += aa_masses[count+1]
			count = 0
		else:
			count += 1
	return result
	
result = problem(data)
print result