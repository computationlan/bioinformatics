"""
Sample Input:
0 0 0 0 4 -2 -3 -1 -7 6 5 3 2 1 9 3 -8 0 3 1 2 1 8

Sample Output:
XZZXX
"""
#aa_masses = {"G": 57, "A": 71, "S": 87, "P": 97, "V": 99, "T": 101, "C": 103, "I": 113, "L": 113, "N": 114, "D": 115, "K": 128, "Q": 128, "E": 129, "M": 131, "H": 137, "F": 147, "R": 156, "Y": 163, "W": 186, "X": 4, "Z": 5}	
#aa_masses = {57: "G", 71: "A", 87: "S", 97: "P", 99: "V", 101: "T", 103: "C", 113: "I", 114: "N", 115: "D", 128: "K", 129: "E", 131: "M", 137: "H", 147: "F", 156: "R", 163: "Y", 186: "W"}	
aa_masses = {4: "X", 5: "Z"}	

input = "0 0 0 0 4 -2 -3 -1 -7 6 5 3 2 1 9 3 -8 0 3 1 2 1 8"
data_list = input.split(" ")
data = []
for each in data_list:
	data.append(int(each))
	
def problem(data):
	graph = []
	for aa in aa_masses:
		for i in range(len(data) - aa):
			graph.append((i, i+aa))
	scores = {i: (0, -float("inf")) for i in range(len(data))}
	scores[0] = (0, 0)
	for node in range(len(data)):
		for each in graph:
			if each[1] == node:	
				new_score = scores[each[0]][1] + data[each[1]]
				if new_score > scores[each[1]][1]: 
					scores[each[1]] = (each[0], new_score)
	result = ""
	i = len(data) - 1
	while i > 1:
		result += aa_masses[i - scores[i][0]]
		i = scores[i][0]
	return result[::-1]
	
result = problem(data)
print result