"""
Sample Input:
4
1
0	13	21	22
13	0	12	13
21	12	0	13
22	13	13	0

Sample Output:
2
"""
raw_data = []
with open("dataset2.txt") as file:
	n_nodes = int(file.readline())
	node_j = int(file.readline())
	while True:
		each = file.readline().rstrip() 		
		if len(each) == 0:
			break
		raw_data.append(each)
data_list = list()		
for each in raw_data:
	list1 = each.split(" ")
	list2 = list()
	for i in list1:
		list2.append(float(i))
	data_list.append(list2)		
print data_list
def limb_length(data_list, n_nodes, node_j):
	min_score = float("inf")
	for i in range(n_nodes):
		for k in range(i, n_nodes):
			d = (data_list[i][node_j] + data_list[node_j][k] - data_list[i][k])/2
			if node_j != i and node_j != k and d < min_score:
				min_score = d
	return min_score
	
print limb_length(data_list, n_nodes, node_j)

