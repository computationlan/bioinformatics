"""
Sample Input:
0 0 0 4 -2 -3 -1 -7 6 5 3 2 1 9 3 -8 0 3 1 2 1 8
XZZXZXXXZXZZXZXXZ
Sample Output:
ZXZXX
score = 24
"""
#aa_masses = {"G": 57, "A": 71, "S": 87, "P": 97, "V": 99, "T": 101, "C": 103, "I": 113, "L": 113, "N": 114, "D": 115, "K": 128, "Q": 128, "E": 129, "M": 131, "H": 137, "F": 147, "R": 156, "Y": 163, "W": 186}	
#aa_masses = {57: "G", 71: "A", 87: "S", 97: "P", 99: "V", 101: "T", 103: "C", 113: "I", 114: "N", 115: "D", 128: "K", 129: "E", 131: "M", 137: "H", 147: "F", 156: "R", 163: "Y", 186: "W"}	
#aa_masses = {4: "X", 5: "Z"}
aa_masses = {"X": 4, "Z": 5}	

input = "0 0 0 0 4 -2 -3 -1 -7 6 5 3 2 1 9 3 -8 0 3 1 2 1 8"
genome = "XZZXZXXXZXZZXZXXZ"
data_list = input.split(" ")
data = []
for each in data_list:
	data.append(int(each))
	
def problem(data, genome):
	score = -float("inf")
	max_len = len(genome) / min(aa_masses.values()) + 1
	for i in range(max_len):
		for k in range(len(genome)-i):
			new_score = 0
			pos = 0
			for m in range(i+1):
				pos += aa_masses[genome[k+m]]
				if pos < len(data):
					new_score += data[pos]
					last = pos
			if new_score > score and last == len(data)-1:
				score = new_score
				best = genome[k:k+i+1]
	return best
	
result = problem(data, genome)
print result