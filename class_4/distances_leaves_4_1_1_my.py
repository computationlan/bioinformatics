"""
Sample Input:
4
0->4:11
1->4:2
2->5:6
3->5:7
4->0:11
4->1:2
4->5:4
5->4:4
5->3:7
5->2:6
Sample Output:
0	13	21	22
13	0	12	13
21	12	0	13
22	13	13	0
"""
def compute_in_degrees(digraph):
    """
    Compute in_degrees
    """
    result = dict()
    for node_2 in digraph:
		for node in digraph[node_2]:
			if node[0] not in result:
				result[node[0]] = [(node_2, node[1])]
			else:
				result[node[0]].append((node_2, node[1]))
    return result

raw_data = []
with open("dataset1.txt") as file:
	n_nodes = int(file.readline())
	while True:
		each = file.readline().rstrip() 		
		if len(each) == 0:
			break
		raw_data.append(each)
data_dict = dict()		
for each in raw_data:
	out_node = int(each.split("->")[0])
	right_side = each.split("->")[1]
	in_node = right_side.split(":")[0]
	length = right_side.split(":")[1]
	if out_node not in data_dict:
		data_dict[out_node] = [(int(in_node), int(length))]
	else:
		data_dict[out_node].append((int(in_node), int(length)))
def scoring(data_dict, n_nodes):
	result_matrix = [[0 for i in range(n_nodes)] for j in range(n_nodes)]
	for n in range(n_nodes):
		check_list = [n]
		node = data_dict[n][0][0]
		score = data_dict[n][0][1]
		check_tuples = {score: data_dict[node][:]}
		check_list.append(node)
		while len(check_tuples) > 0:
			remove_list = []
			for element in check_tuples:
				score = element 
				for tuple in check_tuples[element]:
					if tuple[0] in check_list:
						remove_list.append(tuple)
					if tuple[0] < n_nodes and tuple[0] != n:
						result_matrix[n][tuple[0]] = score + tuple[1]
						remove_list.append(tuple)
				for each in check_tuples:
					for k in remove_list:
						if k in check_tuples[each]:
							check_tuples[each].remove(k)
					remove_dict = []
					if len(check_tuples[each]) == 0:
						remove_dict.append(each)
			for d in remove_dict:
				check_tuples.pop(d)
			if len(check_tuples) > 0:
				new_tuples = dict()
				print score, check_tuples
				for each in check_tuples:
					score = each
					for h in check_tuples[each]:
						check_list.append(h[0])
						score1 = score + h[1]
						if score1 not in new_tuples:
							new_tuples[score1] = data_dict[h[0]][:]
						else:
							new_tuples[score1] += data_dict[h[0]][:]
			check_tuples = new_tuples
	return result_matrix

result_matrix = scoring(data_dict, n_nodes)	
for each in result_matrix:
	print " ".join(str(x) for x in each)