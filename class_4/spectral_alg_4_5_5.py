"""
Sample Input:
XXZ
4 -3 -2 3 3 -4 5 -3 -1 -1 3 4 1 -1
2
Sample Output:
XX(-1)Z(+2)
"""
aa_masses = {"G": 57, "A": 71, "S": 87, "P": 97, "V": 99, "T": 101, "C": 103, "I": 113, "L": 113, "N": 114, "D": 115, "K": 128, "Q": 128, "E": 129, "M": 131, "H": 137, "F": 147, "R": 156, "Y": 163, "W": 186}	
#aa_masses = {57: "G", 71: "A", 87: "S", 97: "P", 99: "V", 101: "T", 103: "C", 113: "I", 114: "N", 115: "D", 128: "K", 129: "E", 131: "M", 137: "H", 147: "F", 156: "R", 163: "Y", 186: "W"}	
#aa_masses = [128, 128, 97, 99, 131, 101, 71, 129, 137, 103, 113, 113, 114, 115, 147, 87, 57, 163, 156, 186]
#aa_masses = {4: "X", 5: "Z"}
#aa_masses = {"X": 4, "Z": 5}

input = "-9 -3 5 1 -3 1 8 -5 1 -4 -8 -10 6 14 -2 8 0 11 -6 8 0 1 4 7 -5 -4 10 6 8 -5 2 -3 12 -3 2 6 -6 2 7 -8 1 9 -8 -4 -2 2 3 13 -1 4 -2 -7 14 -9 2 -5 13 15 15 15 -8 -4 11 12 -3 -7 15 13 -5 5 13 8 -8 -1 1 1 4 -9 11 14 -3 12 4 4 8 3 5 7 3 13 -2 15 13 0 15 1 -5 7 -6 -6 2 11 8 -1 11 -5 11 6 3 9 11 0 -9 4 8 -5 -10 -8 -9 1 6 10 12 12 -10 11 0 9 -8 9 -9 -3 -10 6 -3 -10 11 -2 -5 14 7 7 3 5 4 -2 -7 15 13 6 -8 -3 -9 5 3 15 -10 6 14 5 14 -6 -10 -4 -2 13 3 -8 8 15 -6 4 1 13 1 8 12 3 14 -2 -8 1 5 -7 13 -5 8 1 -2 -3 -9 12 7 -5 -6 15 -9 0 5 12 -6 2 11 15 13 -8 -5 -2 6 -1 5 -5 -7 -9 0 6 5 4 7 3 15 3 -4 9 -2 12 -8 -10 1 5 5 -2 -7 1 10 2 13 7 10 -10 -8 4 1 2 7 -10 9 8 -10 3 -10 15 13 1 4 -3 -1 3 13 -9 0 6 4 7 -8 11 8 -6 -2 -7 1 -3 14 15 4 -6 8 -9 12 -7 0 14 -3 15 -1 10 -10 -2 11 10 12 -2 0 -8 14 -5 -2 -7 11 13 13 13 -4 8 12 7 9 6 -6 10 -6 -10 15 15 -1 0 -3 9 -5 -7 1 -2 13 12 11 -1 8 13 7 1 15 9 2 1 -1 10 9 -5 6 3 7 10 -10 12 7 -3 -4 12 -2 15 -8 -10 0 -1 11 -4 4 2 0 -4 -9 2 -3 4 0 1 10 10 5 5 12 -1 8 -5 5 0 -7 5 -10 -2 9 8 8 10 4 14 3 -3 6 12 -10 8 -6 1 5 -7 9 -8 -7 8 8 -3 -5 -3 -1 8 -5 -9 -9 -8 8 -2 2 -1 -5 11 7 -8 13 7 7 11 -3 -3 -1 8 -3 -10 -7 10 -9 12 5 7 9 9 -5 -4 12 7 -7 14 10 11 -8 13 3 13 -5 4 13 2 0 4 -7 -8 -2 3 7 -5 10 -8 11 -4 6 12 11 0 3 2 14 7 12 0 -5 -3 3 -2 11 0 -5 8 9 5 -6 14 14 11 12 -10 10 11 10 -5 12 11 -1 12 14 7 4 -3 8 3 -7 14 13 5 9 6 5 -6 14 9 10 10 12 -2 7"
errors = 3
data_list = input.split(" ")
data = [0]
for each in data_list:
	data.append(int(each))
peptide = "TSMCP"
	
	
def spectral_alignment(spectrum, peptide, mods):
	s_matrix = [[[-float("inf") for _ in range(len(spectrum))] for _ in range(len(peptide)+1)] for _ in range(mods+1)]
	backtrack = [[[None for _ in range(len(spectrum))] for _ in range(len(peptide)+1)] for _ in range(mods+1)]
	s_matrix[0][0][0] = 0
	for i in range(len(s_matrix)):                 # layer
		for j in range(len(s_matrix[0])):          # peptide
			for k in range(len(s_matrix[0][0])):   # spectrum
				edges = [(i,j-1,k-aa_masses[peptide[j-1]])]
				for mod_mass in range(1,k+1):
					edges.append((i-1,j-1,k-mod_mass))
				for e_i,e_j,e_k in edges:
					if e_i >= 0 and e_j >= 0 and e_k >= 0:
						if s_matrix[i][j][k] < s_matrix[e_i][e_j][e_k]:
							s_matrix[i][j][k] = s_matrix[e_i][e_j][e_k]
							backtrack[i][j][k] = (e_i,e_j,e_k)
				s_matrix[i][j][k] += spectrum[k]
	
	'''for a in s_matrix:
		for b in a:
			print b
		print ""

	for a in backtrack:
		for b in a:
			print b
		print ""'''

	real_masses = []
	i,j,k = len(s_matrix)-1, len(s_matrix[0])-1, len(s_matrix[0][0])-1
	while i > 0 or j > 0 or k > 0:
		old_k = k
		i, j, k = backtrack[i][j][k]
		real_masses.append(old_k-k)
	real_masses = real_masses[::-1]
	
	result = ""
	for i in range(len(peptide)):
		result += peptide[i]
		diff = real_masses[i] - aa_masses[peptide[i]]
		if diff != 0:
			result += '({:+d})'.format(diff)
	return result
	
print spectral_alignment(data, peptide, errors)
	
