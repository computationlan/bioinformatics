"""
Sample Input:
4
TCGGCCAA->4
4->TCGGCCAA
CCTGGCTG->4
4->CCTGGCTG
CACAGGAT->5
5->CACAGGAT
TGAGTACC->5
5->TGAGTACC
4->5
5->4

Sample Output:
17
TCGGCCAA->CCAGGCAC:4
CCTGGCTG->CCAGGCAC:3
TGAGTACC->CAAGGAAC:4
CCAGGCAC->CCTGGCTG:3
CCAGGCAC->CAAGGAAC:2
CCAGGCAC->TCGGCCAA:4
CACAGGAT->CAAGGAAC:4
CAAGGAAC->CACAGGAT:4
CAAGGAAC->TGAGTACC:4
CAAGGAAC->CCAGGCAC:2
"""

from awesome_small_parsimony_4_3_1 import small_parsimony, dist

def read_unrooted_parsimony(file):
	n_leaves = int(file.readline())
	raw_data = file.readlines()
	
	leaf = 0
	graph = {}
	labels = {}
	inv_labels = {}
	for line in raw_data:
		nodes = line.rstrip().split("->")
		if nodes[0].isdigit() and nodes[1].isdigit(): # 1->2
			left = int(nodes[0])
			right = int(nodes[1])
		elif nodes[0].isdigit(): # 1->AAA
			left = int(nodes[0])
			label = nodes[1]
			if label not in inv_labels: 
				labels[leaf] = label
				inv_labels[label] = leaf
				leaf += 1
			right = inv_labels[label]
		else: # AAA->1
			right = int(nodes[1])
			label = nodes[0]
			if label not in inv_labels:
				labels[leaf] = label
				inv_labels[label] = leaf
				leaf += 1
			left = inv_labels[label]
		if left not in graph:
			graph[left] = list()
		graph[left].append(right)
	assert leaf == n_leaves
	for node in graph:
		if node not in labels:
			labels[node] = ''
	return graph, labels
	
def add_root(graph, labels):
	node1 = graph.keys()[0]
	node2 = graph[node1][0]
	root = max(graph.keys()) + 1
	
	directed_graph = {root: [node1, node2]}
	ready = set([node1, node2])
	while len(ready) > 0:
		node = ready.pop()
		directed_children = [child for child in graph[node] if
			child not in directed_graph and child != node1 and child != node2]
		if len(directed_children) > 0:
			directed_graph[node] = directed_children
			ready.update(directed_children)
	labels[root] = ''
	return (directed_graph, labels)
	
def print_parsimony(graph, labels):
	total = 0
	text = ''
	for node in graph:
		for child in graph[node]:
			d = dist(labels[node], labels[child])
			text += "{0}->{1}:{2}\n".format(labels[node], labels[child], d)
			total += d
	print total/2
	print text
	
if __name__ == "__main__":
	with open("dataset11.txt") as file:
		graph, labels = read_unrooted_parsimony(file)

	rooted_graph, labels = add_root(graph, labels)
	labels = small_parsimony(rooted_graph, labels, len(labels[0]))
	print_parsimony(graph, labels)
	