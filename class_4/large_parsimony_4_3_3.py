"""
Sample Input:
5 4
0->4
4->0
1->4
4->1
2->5
5->2
3->5
5->3
4->5
5->4

Sample Output:
1->4
0->5
3->4
2->5
5->2
5->4
5->0
4->1
4->5
4->3

1->5
0->4
3->4
2->5
5->2
5->4
5->1
4->0
4->5
4->3
"""
import copy
raw_data = []
with open("dataset12.txt") as file:
	nodes = file.readline().rstrip().split(" ")
	node1 = int(nodes[0])
	node2 = int(nodes[1])
	while True:
		each = file.readline().rstrip() 		
		if len(each) == 0:
			break
		raw_data.append(each)
data_dict = dict()		
for i in raw_data:
	out_node = int(i.split("->")[0])
	right_side = int(i.split("->")[1])
	if out_node not in data_dict:
		data_dict[out_node] = [right_side]
	else:
		data_dict[out_node].append(right_side)
def large_parsimony(node1, node2, data_dict):
	result = []
	leaves1 = [i for i in data_dict[node2] if i != node1]
	leaves2 = [i for i in data_dict[node1] if i != node2]	
	for j in range(2):
		inter_result = copy.deepcopy(data_dict)
		idx1 = inter_result[node2].index(leaves1[j])
		idx2 = inter_result[node1].index(leaves2[1])
		inter_result[node2][idx1] = leaves2[1]
		inter_result[node1][idx2] = leaves1[j]
		idx3 = inter_result[leaves1[j]].index(node2)
		idx4 = inter_result[leaves2[1]].index(node1)
		inter_result[leaves1[j]][idx3] = node1
		inter_result[leaves2[1]][idx4] = node2
		result.append(inter_result)
	
	return result
	
output = large_parsimony(node1, node2, data_dict)
for each in output:
	for i in each:
		for k in each[i]:
			print str(i)+"->"+str(k)
	print ""

	