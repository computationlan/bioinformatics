"""
Sample Input:
4
0	13	21	22
13	0	12	13
21	12	0	13
22	13	13	0

Sample Output:
0->4:11
1->4:2
2->5:6
3->5:7
4->0:11
4->1:2
4->5:4
5->4:4
5->3:7
5->2:6
"""
raw_data = []
with open("dataset3.txt") as file:
	n_nodes = int(file.readline())
	while True:
		each = file.readline().rstrip() 		
		if len(each) == 0:
			break
		raw_data.append(each)
data_list = list()		
for each in raw_data:
	list1 = each.split(" ")
	list2 = list()
	for i in list1:
		list2.append(float(i))
	data_list.append(list2)		

def limb_length(data_list, n_nodes, node_j):
	min_score = float("inf")
	for i in range(n_nodes):
		for k in range(i, n_nodes):
			d = (data_list[i][node_j] + data_list[node_j][k] - data_list[i][k])/2
			if node_j != i and node_j != k and d < min_score:
				min_score = d
	return min_score

def find_path(graph, prev_node, start_node, end_node):
	if start_node == end_node:
		return [end_node]
	result = None
	for edge in graph[start_node]:
		next_node = edge[0]
		if next_node != prev_node:
			result = find_path(graph, start_node, next_node, end_node)
			if result:
				return [start_node] + result
	return None

def find_location(graph, start_node, end_node, dist):
	path = find_path(graph, None, start_node, end_node)
	for i in range(len(path)):
		edge = None
		for edge1 in graph[path[i]]:
			if edge1[0] == path[i+1]:
				edge = edge1
				break
		edge_dist = edge[1]
		if dist < edge_dist:
			return (path[i], path[i+1], dist)
		dist -= edge_dist

intern_n = len(data_list)

def additive_philogeny(matrix_list, n_nodes):
	if n_nodes == 2:
		return {0: [(1, matrix_list[0][1])], 1: [(0, matrix_list[0][1])] }
	else:
		leaf_j = len(matrix_list[0]) - 1
		limb_leaf = limb_length(matrix_list, (leaf_j+1), leaf_j)
		new_row = []
		for each in matrix_list[-1]:
			new_row.append(each - limb_leaf)
		matrix_list[-1] = new_row[:]
		matrix_list[-1][-1] = 0
		for each in matrix_list[:-1]:
			each[-1] = each[-1] - limb_leaf	
			
		for i in range(len(matrix_list)):
			for k in range(len(matrix_list)):
				if matrix_list[i][k] == matrix_list[i][leaf_j] + matrix_list[k][leaf_j] and k != i and k != leaf_j and i != leaf_j:
					index_i, index_k = i, k
			
		new_matrix = []
		for row in matrix_list[:-1]:
			new_matrix.append(row[:-1])
		graph = additive_philogeny(new_matrix, len(new_matrix[0]))
		global intern_n
		(node_from, node_to, dist) = find_location(graph, index_i, index_k, matrix_list[index_i][leaf_j])
		graph[leaf_j] = [(intern_n, limb_leaf)]
		for gr in graph[node_to]:
			if gr[0] == node_from:
				dist1 = gr[1]
				graph[node_to].remove(gr)
				graph[node_from].remove((node_to, dist1))
		graph[intern_n] = [(leaf_j, limb_leaf), (node_from, dist), (node_to, dist1 - dist)]
		graph[node_from].append((intern_n, dist))
		graph[node_to].append((intern_n, dist1 - dist))
		intern_n += 1
	return graph
result = additive_philogeny(data_list, n_nodes)
print result
for each in result:
	for gr in result[each]:
		print str(each)+"->"+str(gr[0])+":"+str(int(gr[1]))

