"""
Sample Input:
ATATCCG
TCCGA
ATGTACTG

Sample Output:
3
ATATCC-G-
---TCC-GA
ATGTACTG-
"""
seq_v = "ATATCCG"
seq_w = "TCCGA"
seq_t = "ATGTACTG"

def mlcsp(seq_v, seq_w, seq_t):
	s_matrix = [[[0 for d in range(len(seq_t)+1)] for each in range(len(seq_w)+1)] for r in range(len(seq_v)+1)]
	backtrack = dict()
	for i in range(1, len(seq_v)+1):
		for j in range(1, len(seq_w)+1):
			for k in range(1, len(seq_t)+1):
				match = 0
				if seq_v[i-1] == seq_w[j-1] == seq_t[k-1]:
					match = 1
				s_matrix[i][j][k] = max(s_matrix[i-1][j][k], s_matrix[i][j-1][k], s_matrix[i][j][k-1], s_matrix[i-1][j-1][k], s_matrix[i-1][j][k-1], s_matrix[i][j-1][k-1], s_matrix[i-1][j-1][k-1]+match)
				if s_matrix[i][j][k] == s_matrix[i-1][j][k]:
					backtrack[(i, j, k)] = (i-1, j, k)
				elif s_matrix[i][j][k] == s_matrix[i][j-1][k]:
					backtrack[(i, j, k)] = (i, j-1, k)
				elif s_matrix[i][j][k] == s_matrix[i][j][k-1]:
					backtrack[(i, j, k)] = (i, j, k-1)
				elif s_matrix[i][j][k] == s_matrix[i-1][j-1][k]:
					backtrack[(i, j, k)] = (i-1, j-1, k)
				elif s_matrix[i][j][k] == s_matrix[i-1][j][k-1]:
					backtrack[(i, j, k)] = (i-1, j, k-1)
				elif s_matrix[i][j][k] == s_matrix[i][j-1][k-1]:
					backtrack[(i, j, k)] = (i, j-1, k-1)
				elif s_matrix[i][j][k] == s_matrix[i-1][j-1][k-1]+match:
					backtrack[(i, j, k)] = (i-1, j-1, k-1)
			 		  
	#for k in s_matrix:
	#	print k
	print s_matrix[-1][-1][-1]
	return backtrack
	
backtrack =  mlcsp(seq_v, seq_w, seq_t)
i = len(seq_v)
j = len(seq_w)
k = len(seq_t)


def fit(backtrack, seq_v, seq_w, seq_t, i, j, k):	
	LCS_v = list()
	LCS_w = list()
	LCS_t = list()
	while i > 0 and j > 0 and k > 0:
		old_i, old_j, old_k  = i, j, k
		i, j, k = backtrack[(i, j, k)]
		LCS_v.append("-" if old_i == i else seq_v[i])
		LCS_w.append("-" if old_j == j else seq_w[j])
		LCS_t.append("-" if old_k == k else seq_t[k])
	LCS_v.reverse()
	LCS_w.reverse()
	LCS_t.reverse()
	print "".join(LCS_v)
	print "".join(LCS_w)
	print "".join(LCS_t)

fit(backtrack, seq_v, seq_w, seq_t, i, j, k)