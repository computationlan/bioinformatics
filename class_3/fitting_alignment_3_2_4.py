"""
Sample Input:
GTAGGCTTAAGGTTA
TAGATA
Sample Output:
2
TAGGCTTA
TAGA--TA
"""
seq_v = "GTAGGCTTAAGGTTA"
seq_w = "TAGATA"

def fitting_alignment(seq_v, seq_w):
	s_matrix = [[0 for each in range(len(seq_v)+1)] for r in range(len(seq_w)+1)]
	for i in range(1, len(seq_w)+1):
		s_matrix[i][0] = s_matrix[i-1][0]-1
	backtrack = dict()
	#max_s = 0
	for i in range(1, len(seq_v)+1):
		for j in range(1, len(seq_w)+1):
			match = -1
			if seq_v[i-1] == seq_w[j-1]:
				match = 1
			s_matrix[j][i] = max((s_matrix[j-1][i]-1), (s_matrix[j][i-1]-1), (s_matrix[j-1][i-1]+match))
			if s_matrix[j][i] == s_matrix[j-1][i]-1:
				backtrack[(j, i)] = (i, j-1)
			elif s_matrix[j][i] == s_matrix[j][i-1]-1:
				backtrack[(j, i)] = (i-1, j)
			elif s_matrix[j][i] == s_matrix[j-1][i-1] + match:
				backtrack[(j, i)] = (i-1, j-1)	
			  
	#for k in s_matrix:
		#print k
	max_s = max(s_matrix[-1])
	print max_s
	max_i = s_matrix[-1].index(max_s)
	max_j = len(seq_w)
	return backtrack, max_i, max_j

def fit(backtrack, seq_v, seq_w, i, j):
	LCS_v = list()
	LCS_w = list()
	while i > 0 and j > 0:
		old_i, old_j = i, j
		i, j = backtrack[(j, i)]
		LCS_v.append("-" if old_i == i else seq_v[i])
		LCS_w.append("-" if old_j == j else seq_w[j])
	LCS_v.reverse()
	LCS_w.reverse()
	print "".join(LCS_v)
	print "".join(LCS_w)

backtrack, i, j = fitting_alignment(seq_v, seq_w)
fit(backtrack, seq_v, seq_w, i, j)
