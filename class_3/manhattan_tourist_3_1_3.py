"""
Sample Input
4 4
1 0 2 4 3
4 6 5 2 1
4 4 5 2 1
5 6 8 5 3
-
3 2 4 0
3 2 4 2
0 7 3 3
3 3 0 2
1 3 2 2

Sample Output
34
Input Integers n and m, followed by an n x (m + 1) matrix 
Down and an (n + 1) x m matrix Right.
The two matrices are separated by the - symbol.
Output The length of a longest path from source (0, 0) to 
sink (n, m) in the n x m rectangular grid
whose edges are defined by the matrices Down and Right.
1 = n = down
2 = m = right
"""
raw_data = []
with open("datatext.txt") as file:
	while True:
		each = file.readline().rstrip() 		
		if len(each) == 0:
			break
		raw_data.append(each)
n_m = raw_data[0].split(" ")
n = int(n_m[0])
m = int(n_m[1])
down_matrix = []
for i in range(n):
	line_list = []
	line = raw_data[1+i].split(" ")
	for each in range(len(line)):
		line_list.append(int(line[each]))
	down_matrix.append(line_list)
right_matrix = []
for i in range(n+1):
	line_list = []
	line = raw_data[2+n+i].split(" ")
	for each in range(len(line)):
		line_list.append(int(line[each]))
	right_matrix.append(line_list)

	
def manhattan_tourist(n, m, down_matrix, right_matrix):
	s_matrix = [[0 for each in range(m+1)] for r in range(n+1)]
	for k in s_matrix:
		print k
	for i in range(1, n+1):
		s_matrix[i][0] = s_matrix[i-1][0]+down_matrix[i-1][0]
	for j in range(1, m+1):
		s_matrix[0][j] = s_matrix[0][j-1]+right_matrix[0][j-1]
	for i in range(1, n+1):
		for j in range(1, m+1):
			s_matrix[i][j] = max((s_matrix[i-1][j]+down_matrix[i-1][j]), (s_matrix[i][j-1]+right_matrix[i][j-1]))
		
	for k in s_matrix:
		print k
	return s_matrix[-1][-1]
		
print manhattan_tourist(n, m, down_matrix, right_matrix)

