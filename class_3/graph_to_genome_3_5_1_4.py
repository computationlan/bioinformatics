"""
Sample Input:
(2, 4), (3, 6), (5, 1), (7, 9), (10, 12), (11, 8)
Sample Output:
(+1 -2 -3)(-4 +5 -6)
"""

permutation = "2, 4), (3, 6), (5, 1), (7, 9), (10, 12), (11, 8".split("), (")
genome_graph = []
for each in permutation:
	new = each.split(", ")
	data_list = []
	for i in new:
		data_list.append(int(i))
	genome_graph.append(data_list)

def ct_ch(nodes, num):
	chromosome = []
	count = 2*num - 1
	for j in range(len(nodes)/2):
		if nodes[2*j] == count:
			chromosome.append(-(count+1)/2)
		else:
			chromosome.append((count+1)/2)
		count += 2
	result = ""
	for each in chromosome:
		if each > 0:
			result += "+"+str(each)+" "
		else:
			result += str(each)+ " "
	return result
	
def graph_to_genome(genome_graph):
	chromosomes = []
	current = []
	num = 1
	for each in genome_graph:
		current.append(each[0])
		current.append(each[1])
		if each[0] > each[1]:
			chromosomes.append("("+ct_ch(current, num)[:-1]+")")
			num += len(current)/2
			current = []
	return chromosomes

result = graph_to_genome(genome_graph)
print_out = ""
for each in result:
	print_out += each
print print_out