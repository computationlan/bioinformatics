"""
Sample Input:
AACCTTGG
ACACTGTGA

Sample Output:
AACTGG
"""
seq_v = "AACCTTGG"
seq_w = "ACACTGTGA"

def lcs_backtrack(seq_v, seq_w):
	s_matrix = [[0 for each in range(len(seq_v)+1)] for r in range(len(seq_w)+1)]
	backtrack = dict()
	for i in range(1, len(seq_v)+1):
		for j in range(1, len(seq_w)+1):
			match = 0
			if seq_v[i-1] == seq_w[j-1]:
				match = 1
			s_matrix[j][i] = max(s_matrix[j-1][i], s_matrix[j][i-1], (s_matrix[j-1][i-1]+match))
			if s_matrix[j][i] == s_matrix[j-1][i]:
				backtrack[(j, i)] = "right"
			elif s_matrix[j][i] == s_matrix[j][i-1]:
				backtrack[(j, i)] = "down"
			elif s_matrix[j][i] == s_matrix[j-1][i-1] + 1 and seq_v[i-1] == seq_w[j-1]:
				backtrack[(j, i)] = "diagonal" 		 
	
	#for k in s_matrix:
		#print k
	return backtrack

backtrack = lcs_backtrack(seq_v, seq_w)
#print backtrack

def output_lcs(backtrack, seq_v, j, i):
	if i == 0 or j == 0:
		return
	if backtrack[(j, i)]  == "diagonal":
		output_lcs(backtrack, seq_v, (j-1), (i-1))
		print seq_v[i-1],
	elif backtrack[(j, i)] == "right":
		output_lcs(backtrack, seq_v, (j-1), i)
	else:
		output_lcs(backtrack, seq_v, j, (i-1))

	
i = len(seq_v)
j = len(seq_w)
output_lcs(backtrack, seq_v, j, i)
print""
LCS = list()
while i > 0 and j > 0:
	if backtrack[(j, i)] == "diagonal":
		LCS.append(seq_v[i-1])
		i = i - 1
		j = j - 1
	elif backtrack[(j, i)] == "right":
		j = j - 1
	else:
		i = i - 1
LCS.reverse()
print "".join(LCS)

"""def output_lcs(backtrack, seq_v, j, i):
	if i == 0 or j == 0:
		return
	if backtrack[(i, j)]  == "down":
		output_lcs(backtrack, seq_v, (j-1), i)
	elif backtrack[(i, j)] == "right":
		output_lcs(backtrack, seq_v, j, (i-1))
	else:
		output_lcs(backtrack, seq_v, (j-1), (i-1))
		print seq_v[i],"""

