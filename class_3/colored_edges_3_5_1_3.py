"""
Sample Input:
(+1 -2 -3)(+4 +5 -6)
Sample Output:
(2, 4), (3, 6), (5, 1), (8, 9), (10, 12), (11, 7)
"""

permutation = "-2 -3)(+4 +5 -6".split(")(")
chromosomes = []
for each in permutation:
	new = each.split()
	data_list = []
	for i in new:
		data_list.append(int(i))
	chromosomes.append(data_list)

def ctc(chromosome):
	nodes = []
	for j in range(len(chromosome)):
		i = chromosome[j]
		if i > 0:
			nodes.append(2*i - 1)
			nodes.append(2*i)
		else:
			nodes.append(-2*i)
			nodes.append(-2*i-1)
			
	return nodes
	
def colored_edges(chromosomes):
	edges = []
	for each in chromosomes:
		nodes = ctc(each)
		for j in range(len(each)-1):
			edges.append((nodes[2*j+1], nodes[2*j+2]))
		edges.append((nodes[-1], nodes[0]))
			
	return edges
	
print colored_edges(chromosomes)