"""
Sample Input:
(+1 -2 -3 +4)
Sample Output:
(1 2 4 3 6 5 7 8)
"""

permutation = "+1 -2 -3 +4".split()
data_list = []
for each in permutation:
	data_list.append(int(each))
print data_list

def ctc(chromosome):
	nodes = []
	for j in range(len(chromosome)):
		i = chromosome[j]
		if i > 0:
			nodes.append(2*i - 1)
			nodes.append(2*i)
		else:
			nodes.append(-2*i)
			nodes.append(-2*i-1)
			
	return nodes
	
result = ctc(data_list)
for each in result:
	print each,

