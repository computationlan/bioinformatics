"""
Sample Input:
PLEASANTLY
MEASNLY

Sample Output:
(4, 3) (5, 4)
"""
seq_w = "PLEASANTLY"
seq_v = "MEASNLY"

blosum_index = ["A", "C", "D", "E", "F", "G", "H", "I", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "V", "W", "Y"]
blosum_62 = [[4,  0, -2, -1, -2,  0, -2, -1, -1, -1, -1, -2, -1, -1, -1,  1,  0,  0, -3, -2], 
[0,  9, -3, -4, -2, -3, -3, -1, -3, -1, -1, -3, -3, -3, -3, -1, -1, -1, -2, -2],
[-2, -3,  6,  2, -3, -1, -1, -3, -1, -4, -3,  1, -1,  0, -2,  0, -1, -3, -4, -3],
[-1, -4,  2,  5, -3, -2,  0, -3,  1, -3, -2,  0, -1,  2,  0,  0, -1, -2, -3, -2],
[-2, -2, -3, -3,  6, -3, -1,  0, -3,  0,  0, -3, -4, -3, -3, -2, -2, -1,  1,  3],
[0, -3, -1, -2, -3,  6, -2, -4, -2, -4, -3,  0, -2, -2, -2,  0, -2, -3, -2, -3],
[-2, -3, -1,  0, -1, -2,  8, -3, -1, -3, -2,  1, -2,  0,  0, -1, -2, -3, -2,  2],
[-1, -1, -3, -3,  0, -4, -3,  4, -3,  2,  1, -3, -3, -3, -3, -2, -1,  3, -3, -1],
[-1, -3, -1,  1, -3, -2, -1, -3,  5, -2, -1,  0, -1,  1,  2,  0, -1, -2, -3, -2],
[-1, -1, -4, -3,  0, -4, -3,  2, -2,  4,  2, -3, -3, -2, -2, -2, -1,  1, -2, -1],
[-1, -1, -3, -2,  0, -3, -2,  1, -1,  2,  5, -2, -2,  0, -1, -1, -1,  1, -1, -1],
[-2, -3,  1,  0, -3,  0,  1, -3,  0, -3, -2,  6, -2,  0,  0,  1,  0, -3, -4, -2],
[-1, -3, -1, -1, -4, -2, -2, -3, -1, -3, -2, -2,  7, -1, -2, -1, -1, -2, -4, -3],
[-1, -3,  0,  2, -3, -2,  0, -3,  1, -2,  0,  0, -1,  5,  1,  0, -1, -2, -2, -1],
[-1, -3, -2,  0, -3, -2,  0, -3,  2, -2, -1,  0, -2,  1,  5, -1, -1, -3, -3, -2],
[1, -1,  0,  0, -2,  0, -1, -2,  0, -2, -1,  1, -1,  0, -1,  4,  1, -2, -3, -2],
[0, -1, -1, -1, -2, -2, -2, -1, -1, -1, -1,  0, -1, -1, -1,  1,  5,  0, -2, -2],
[0, -1, -3, -2, -1, -3, -3,  3, -2,  1,  1, -3, -2, -2, -3, -2,  0,  4, -3, -1],
[-3, -2, -4, -3,  1, -2, -2, -3, -3, -2, -1, -4, -4, -2, -3, -3, -2, -3, 11,  2],
[-2, -2, -3, -2,  3, -3,  2, -1, -2, -1, -1, -2, -3, -1, -2, -2, -2, -1,  2,  7]]
indel = 5

def middle_edge(seq_v, seq_w, col_idx):
	left_col = [0]
	for each in range(1, len(seq_w)+1):
		left_col.append(left_col[each-1] - indel)
	for n in range(1, col_idx+1):
		new_col = [left_col[0]-indel]
		for i in range(1, len(seq_w)+1):
			match = blosum_62[blosum_index.index(seq_v[n-1])][blosum_index.index(seq_w[i-1])]
			s = max((new_col[i-1]-indel), (left_col[i]-indel), (left_col[i-1]+match))
			new_col.append(s)
		left_col = new_col[:]
	
	right_col = [0]
	for each in range(1, len(seq_w)+1):
		right_col.append(right_col[each-1] - indel)
	for n in range(1, len(seq_v)-col_idx+1):
		new_col = [right_col[0] - indel]
		for i in range(1, len(seq_w)+1):
			match = blosum_62[blosum_index.index(seq_v[-n])][blosum_index.index(seq_w[-i])]
			s = max((new_col[i-1]-indel), (right_col[i]-indel), (right_col[i-1]+match))
			new_col.append(s)
		right_col = new_col[:]
	right_col = right_col[::-1]
	
	matrix_col = []
	max_s = - float("inf")
	for g in range(len(seq_w)+1):
		element = left_col[g]+right_col[g]
		matrix_col.append(element)
		if element >= max_s:
			max_s = element
			ind_v = g
			
	return (ind_v, col_idx)
	
print middle_edge(seq_v, seq_w, len(seq_v)/2)
print middle_edge(seq_v, seq_w, len(seq_v)/2+1)