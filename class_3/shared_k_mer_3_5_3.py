"""
Sample Input:
3
AAACTCATC
TTTCAAATC
Sample Output:
(0, 4)
(0, 0)
(4, 2)
(6, 6)
"""
seq_v = "AAACTCATC"
seq_w = "TTTCAAATC"
r_w = "GATTTGAAA" 
k = 3

def shared(seq_v, seq_w, r_w, k):
	result = []
	dict_v = dict()
	for i in range(len(seq_v)-k+1):
		if seq_v[i:i+k] not in dict_v:
			dict_v[seq_v[i:i+k]] = [i]
		else:
			dict_v[seq_v[i:i+k]].append(i)
	dict_w = dict()	 
	len_r = len(r_w)
	for j in range(len(seq_w)-k+1):
		if seq_w[j:j+k] not in dict_w:
			dict_w[seq_w[j:j+k]] = [j]
		else:
			dict_w[seq_w[j:j+k]].append(j)
	for m in range(len(seq_w)-k+1):
		if r_w[m:m+k] not in dict_w:
			dict_w[r_w[m:m+k]] = [len_r - m - k]
		else:
			dict_w[r_w[m:m+k]].append(len_r - m - k) 
	for each in dict_v:
		if each in dict_w:
			result.append((dict_v[each], dict_w[each]))
	return result
	
result =  shared(seq_v, seq_w, r_w, k)
for each in result:
	left = each[0]
	right = each[1]
	for i in left:
		for n in right:
			print (i, n)
	