"""
Sample Input:
PLEASANTLY
MEANLY

Sample Output:
8
PLEASANTLY
-MEA--N-LY
"""
seq_w = "PLEASANTLY"
seq_v = "MEANLY"

blosum_index = ["A", "C", "D", "E", "F", "G", "H", "I", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "V", "W", "Y"]
blosum_62 = [[4,  0, -2, -1, -2,  0, -2, -1, -1, -1, -1, -2, -1, -1, -1,  1,  0,  0, -3, -2], 
[0,  9, -3, -4, -2, -3, -3, -1, -3, -1, -1, -3, -3, -3, -3, -1, -1, -1, -2, -2],
[-2, -3,  6,  2, -3, -1, -1, -3, -1, -4, -3,  1, -1,  0, -2,  0, -1, -3, -4, -3],
[-1, -4,  2,  5, -3, -2,  0, -3,  1, -3, -2,  0, -1,  2,  0,  0, -1, -2, -3, -2],
[-2, -2, -3, -3,  6, -3, -1,  0, -3,  0,  0, -3, -4, -3, -3, -2, -2, -1,  1,  3],
[0, -3, -1, -2, -3,  6, -2, -4, -2, -4, -3,  0, -2, -2, -2,  0, -2, -3, -2, -3],
[-2, -3, -1,  0, -1, -2,  8, -3, -1, -3, -2,  1, -2,  0,  0, -1, -2, -3, -2,  2],
[-1, -1, -3, -3,  0, -4, -3,  4, -3,  2,  1, -3, -3, -3, -3, -2, -1,  3, -3, -1],
[-1, -3, -1,  1, -3, -2, -1, -3,  5, -2, -1,  0, -1,  1,  2,  0, -1, -2, -3, -2],
[-1, -1, -4, -3,  0, -4, -3,  2, -2,  4,  2, -3, -3, -2, -2, -2, -1,  1, -2, -1],
[-1, -1, -3, -2,  0, -3, -2,  1, -1,  2,  5, -2, -2,  0, -1, -1, -1,  1, -1, -1],
[-2, -3,  1,  0, -3,  0,  1, -3,  0, -3, -2,  6, -2,  0,  0,  1,  0, -3, -4, -2],
[-1, -3, -1, -1, -4, -2, -2, -3, -1, -3, -2, -2,  7, -1, -2, -1, -1, -2, -4, -3],
[-1, -3,  0,  2, -3, -2,  0, -3,  1, -2,  0,  0, -1,  5,  1,  0, -1, -2, -2, -1],
[-1, -3, -2,  0, -3, -2,  0, -3,  2, -2, -1,  0, -2,  1,  5, -1, -1, -3, -3, -2],
[1, -1,  0,  0, -2,  0, -1, -2,  0, -2, -1,  1, -1,  0, -1,  4,  1, -2, -3, -2],
[0, -1, -1, -1, -2, -2, -2, -1, -1, -1, -1,  0, -1, -1, -1,  1,  5,  0, -2, -2],
[0, -1, -3, -2, -1, -3, -3,  3, -2,  1,  1, -3, -2, -2, -3, -2,  0,  4, -3, -1],
[-3, -2, -4, -3,  1, -2, -2, -3, -3, -2, -1, -4, -4, -2, -3, -3, -2, -3, 11,  2],
[-2, -2, -3, -2,  3, -3,  2, -1, -2, -1, -1, -2, -3, -1, -2, -2, -2, -1,  2,  7]]
indel = 5

def middle_edge(seq_v, seq_w, col_idx):
	left_col = [0]
	for each in range(1, len(seq_w)+1):
		left_col.append(left_col[each-1] - indel)
	for n in range(1, col_idx+1):
		new_col = [left_col[0]-indel]
		for i in range(1, len(seq_w)+1):
			match = blosum_62[blosum_index.index(seq_v[n-1])][blosum_index.index(seq_w[i-1])]
			s = max((new_col[i-1]-indel), (left_col[i]-indel), (left_col[i-1]+match))
			new_col.append(s)
		left_col = new_col[:]
	
	right_col = [0]
	for each in range(1, len(seq_w)+1):
		right_col.append(right_col[each-1] - indel)
	for n in range(1, len(seq_v)-col_idx+1):
		new_col = [right_col[0] - indel]
		for i in range(1, len(seq_w)+1):
			match = blosum_62[blosum_index.index(seq_v[-n])][blosum_index.index(seq_w[-i])]
			s = max((new_col[i-1]-indel), (right_col[i]-indel), (right_col[i-1]+match))
			new_col.append(s)
		right_col = new_col[:]
	right_col = right_col[::-1]
	
	matrix_col = []
	max_s = - float("inf")
	for g in range(len(seq_w)+1):
		element = left_col[g]+right_col[g]
		matrix_col.append(element)
		if element >= max_s:
			max_s = element
			ind_v = g
	#print max_s
			
	return max_s, (ind_v, col_idx)

def global_score(seq1, seq2):
	assert len(seq1) == len(seq2)
	score = 0
	for a,b in zip(seq1, seq2):
		if a=='-' or b=='-':
			score -= indel
		else:
			score += blosum_62[blosum_index.index(a)][blosum_index.index(b)]
	return score

def lsa(seq_v, seq_w):
	if len(seq_w) == 0:
		return (seq_v, '-'*len(seq_v))
	elif len(seq_v) == 0:
		return ('-'*len(seq_w),seq_w)
	elif len(seq_v) == 1:
		best_score = -float('inf')
		for i in range(len(seq_w)):
			score = blosum_62[blosum_index.index(seq_v[0])][blosum_index.index(seq_w[i])]
			if score > best_score:
				best_score = score
				best_pos = i
		return ('-'*best_pos+seq_v+'-'*(len(seq_w)-best_pos-1), seq_w)
	else:
		_, mid = middle_edge(seq_v, seq_w, len(seq_v)/2)
		left = lsa(seq_v[:mid[1]], seq_w[:mid[0]]) 
		right = lsa(seq_v[mid[1]:], seq_w[mid[0]:])
		return (left[0]+right[0], left[1]+right[1])
	
sol = lsa(seq_v, seq_w)
print global_score(sol[0], sol[1])
print sol[1]
print sol[0]

#print middle_edge(seq_v, seq_w, len(seq_v)/2)
