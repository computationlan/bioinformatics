"""
Sample Input:
PAWHEAE
HEAGAWGHEE
Sample Output:
1
HEAE
HEAG
"""
seq_v = "PAWHEAE"
seq_w = "HEAGAWGHEE"

def overlap_alignment(seq_v, seq_w):
	s_matrix = [[0 for each in range(len(seq_v)+1)] for r in range(len(seq_w)+1)]
	for i in range(1, len(seq_w)+1):
		s_matrix[i][0] = s_matrix[i-1][0]-2
	backtrack = dict()
	for i in range(1, len(seq_v)+1):
		for j in range(1, len(seq_w)+1):
			match = -2
			if seq_v[i-1] == seq_w[j-1]:
				match = 1
			s_matrix[j][i] = max((s_matrix[j-1][i]-2), (s_matrix[j][i-1]-2), (s_matrix[j-1][i-1]+match))
			if s_matrix[j][i] == s_matrix[j-1][i]-2:
				backtrack[(j, i)] = (i, j-1)
			elif s_matrix[j][i] == s_matrix[j][i-1]-2:
				backtrack[(j, i)] = (i-1, j)
			elif s_matrix[j][i] == s_matrix[j-1][i-1] + match:
				backtrack[(j, i)] = (i-1, j-1)	
			  
	#for k in s_matrix:
		#print k
	max_s = 0
	pos = 0
	for s in range(len(seq_w)):
		if s_matrix[s][-1] >= max_s:
			max_s = s_matrix[s][-1]
			pos = s
	return backtrack, max_s, pos

def fit(backtrack, seq_v, seq_w, i, j):
	LCS_v = list()
	LCS_w = list()
	while i > 0 and j > 0:
		old_i, old_j = i, j
		i, j = backtrack[(j, i)]
		LCS_v.append("-" if old_i == i else seq_v[i])
		LCS_w.append("-" if old_j == j else seq_w[j])
	LCS_v.reverse()
	LCS_w.reverse()
	print "".join(LCS_v)
	print "".join(LCS_w)

backtrack, max_s, pos = overlap_alignment(seq_v, seq_w)
print max_s
fit(backtrack, seq_v, seq_w, len(seq_v), pos)