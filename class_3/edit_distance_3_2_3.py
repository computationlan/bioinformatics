"""
Sample Input:
PLEASANTLY
MEANLY

Sample Output:
5
"""

seq_v = "PLEASANTLY"
seq_w = "MEANLY"
def lcs_backtrack(seq_v, seq_w):
	s_matrix = [[0 for each in range(len(seq_v)+1)] for r in range(len(seq_w)+1)]
	s_matrix[0] = [k for k in range(len(seq_v)+1)]
	for m in range(len(seq_w)+1):
		s_matrix[m][0] = m
	backtrack = dict()
	for i in range(1, len(seq_v)+1):
		for j in range(1, len(seq_w)+1):
			match = 1
			if seq_v[i-1] == seq_w[j-1]:
				match = 0
			s_matrix[j][i] = min(s_matrix[j-1][i]+1, s_matrix[j][i-1]+1, (s_matrix[j-1][i-1]+match))
			"""if s_matrix[j][i] == s_matrix[j-1][i]:
				backtrack[(j, i)] = "right"
			elif s_matrix[j][i] == s_matrix[j][i-1]:
				backtrack[(j, i)] = "down"
			elif s_matrix[j][i] == s_matrix[j-1][i-1] + 1 and seq_v[i-1] == seq_w[j-1]:
				backtrack[(j, i)] = "diagonal" 	
				"""	 
	
	for k in s_matrix:
		print k
	return backtrack

backtrack = lcs_backtrack(seq_v, seq_w)
	
i = len(seq_v)
j = len(seq_w)

"""LCS = list()
while i > 0 and j > 0:
	if backtrack[(j, i)] == "diagonal":
		LCS.append(seq_v[i-1])
		i = i - 1
		j = j - 1
	elif backtrack[(j, i)] == "right":
		j = j - 1
	else:
		i = i - 1
LCS.reverse()
print "".join(LCS)