def dp_change(money, coins):
	coins = sorted(coins, reverse=True)
	min_num_coins = {k: float("inf") for k in range(1, (money+1))}
	min_num_coins[0] = 0
	for m in range(1, (money+1)):
		for coin in coins:
			if m >= coin:
				if min_num_coins[(m - coin)] + 1 < min_num_coins[m]:
					min_num_coins[m] = min_num_coins[m - coin] + 1
	return min_num_coins[money]

def dp_change_2(money, coins):
	coins = sorted(coins, reverse=True)
	min_num_coins = {k: float("inf") for k in range(coins[0])}
	min_num_coins[0] = 0
	for m in range(1, (money+1)):
		min_num_coins[m%coins[0]] += 1
		for coin in coins[1:]:
			if m >= coin:
				if min_num_coins[(m - coin)%coins[0]] + 1 < min_num_coins[m%coins[0]]:
					min_num_coins[m%coins[0]] = min_num_coins[(m - coin)%coins[0]] + 1
	return min_num_coins[money%coins[0]]
	
def dp_change_3(money, coins):
	coins = sorted(coins, reverse=True)
	min_num_coins = {k: float("inf") for k in range(1, (money+1))}
	min_num_coins[0] = 0
	min_coin_sol = {k: () for k in range(money+1)}
	for m in range(1, (money+1)):
		for coin in coins:
			if m >= coin:
				if min_num_coins[(m - coin)] + 1 < min_num_coins[m]:
					min_num_coins[m] = min_num_coins[m - coin] + 1
					min_coin_sol[m] = min_coin_sol[m - coin] + (coin,)
	return min_coin_sol[money]
	
money = 8074
coins_input = "24,13,12,7,5,3,1"
coins = [int(each) for each in coins_input.split(",")]
print dp_change(money, coins)
print dp_change_3(money, coins)


money = 52
coins_input = "10,4"
coins = [int(each) for each in coins_input.split(",")]
print dp_change_3(money, coins)
print dp_change(money, coins)