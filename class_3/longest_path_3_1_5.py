"""
Sample Input:
0
4
0->1:7
0->2:4
2->3:2
1->4:1
3->4:3

Sample Output:
9
0->2->3->4
"""
def compute_in_degrees(digraph):
    """
    Compute in_degrees
    """
    result = dict()
    for node_2 in digraph:
		for node in digraph[node_2]:
			if node[0] not in result:
				result[node[0]] = [(node_2, node[1])]
			else:
				result[node[0]].append((node_2, node[1]))
    return result

raw_data = []
with open("dataset.txt") as file:
	start = int(file.readline())
	end = int(file.readline())
	while True:
		each = file.readline().rstrip() 		
		if len(each) == 0:
			break
		raw_data.append(each)
data_dict = dict()		
for each in raw_data:
	out_node = int(each.split("->")[0])
	right_side = each.split("->")[1]
	in_node = right_side.split(":")[0]
	length = right_side.split(":")[1]
	if out_node not in data_dict:
		data_dict[out_node] = [(int(in_node), int(length))]
	else:
		data_dict[out_node].append((int(in_node), int(length)))
def scoring(data_dict, start, end):
	in_degrees = compute_in_degrees(data_dict)
	best_score = {k:0 for k in range(end+1)}
	last_element = {start:0}
	for node in range(end+1):
		if node not in in_degrees:
			pass
		else:
			for each in in_degrees[node]:
				if best_score[each[0]] + each[1] > best_score[node] and each[0] in last_element:
					best_score[node] = best_score[each[0]] + each[1]
					last_element[node] = each[0]
	return best_score, last_element

def get_path(last_element, start, end):
	if start == end:
		return [end]
	else:
		return get_path(last_element, start, last_element[end]) + [end]

best_score, last_element = scoring(data_dict, start, end)	
print best_score[end]		
path = get_path(last_element, start, end)
print "->".join(str(x) for x in path)


				


def longest(start, end, data_dict):
	for each in data_dict[start]:
		if each[0] == end:
			return each[1]
		else:
			return longest(each[0], end, data_dict) + each[1]
#print longest(0, 4, data_dict) 

def maxs_compute_in_degrees(digraph):
    """
    Compute in_degrees
    """
    result = dict()
    for node_2 in digraph:
		for node in digraph[node_2]:
			if node[0] not in result:
				result[node[0]] = 0
			result[node[0]] += 1
    return result