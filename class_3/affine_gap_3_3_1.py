"""
Sample Input:
PRTEINS
PRTWPSEIN

Sample Output:
8
PRT---EINS
PRTWPSEIN-
"""
seq_v = "PRTEINS"
seq_w = "PRTWPSEIN"

blosum_index = ["A", "C", "D", "E", "F", "G", "H", "I", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "V", "W", "Y"]
blosum_62 = [[4,  0, -2, -1, -2,  0, -2, -1, -1, -1, -1, -2, -1, -1, -1,  1,  0,  0, -3, -2], 
[0,  9, -3, -4, -2, -3, -3, -1, -3, -1, -1, -3, -3, -3, -3, -1, -1, -1, -2, -2],
[-2, -3,  6,  2, -3, -1, -1, -3, -1, -4, -3,  1, -1,  0, -2,  0, -1, -3, -4, -3],
[-1, -4,  2,  5, -3, -2,  0, -3,  1, -3, -2,  0, -1,  2,  0,  0, -1, -2, -3, -2],
[-2, -2, -3, -3,  6, -3, -1,  0, -3,  0,  0, -3, -4, -3, -3, -2, -2, -1,  1,  3],
[0, -3, -1, -2, -3,  6, -2, -4, -2, -4, -3,  0, -2, -2, -2,  0, -2, -3, -2, -3],
[-2, -3, -1,  0, -1, -2,  8, -3, -1, -3, -2,  1, -2,  0,  0, -1, -2, -3, -2,  2],
[-1, -1, -3, -3,  0, -4, -3,  4, -3,  2,  1, -3, -3, -3, -3, -2, -1,  3, -3, -1],
[-1, -3, -1,  1, -3, -2, -1, -3,  5, -2, -1,  0, -1,  1,  2,  0, -1, -2, -3, -2],
[-1, -1, -4, -3,  0, -4, -3,  2, -2,  4,  2, -3, -3, -2, -2, -2, -1,  1, -2, -1],
[-1, -1, -3, -2,  0, -3, -2,  1, -1,  2,  5, -2, -2,  0, -1, -1, -1,  1, -1, -1],
[-2, -3,  1,  0, -3,  0,  1, -3,  0, -3, -2,  6, -2,  0,  0,  1,  0, -3, -4, -2],
[-1, -3, -1, -1, -4, -2, -2, -3, -1, -3, -2, -2,  7, -1, -2, -1, -1, -2, -4, -3],
[-1, -3,  0,  2, -3, -2,  0, -3,  1, -2,  0,  0, -1,  5,  1,  0, -1, -2, -2, -1],
[-1, -3, -2,  0, -3, -2,  0, -3,  2, -2, -1,  0, -2,  1,  5, -1, -1, -3, -3, -2],
[1, -1,  0,  0, -2,  0, -1, -2,  0, -2, -1,  1, -1,  0, -1,  4,  1, -2, -3, -2],
[0, -1, -1, -1, -2, -2, -2, -1, -1, -1, -1,  0, -1, -1, -1,  1,  5,  0, -2, -2],
[0, -1, -3, -2, -1, -3, -3,  3, -2,  1,  1, -3, -2, -2, -3, -2,  0,  4, -3, -1],
[-3, -2, -4, -3,  1, -2, -2, -3, -3, -2, -1, -4, -4, -2, -3, -3, -2, -3, 11,  2],
[-2, -2, -3, -2,  3, -3,  2, -1, -2, -1, -1, -2, -3, -1, -2, -2, -2, -1,  2,  7]]
sigma = 11
eps = 1

def overlap_alignment(seq_v, seq_w):
	s_matrix = [[[0, 0, 0] for each in range(len(seq_v)+1)] for r in range(len(seq_w)+1)]
	backtrack = dict()
	for i in range(len(seq_v)+1):
		for j in range(len(seq_w)+1):
			# lower(j,i) = max(lower(j-1,i)-eps,mid(j-1,i)-sigma)
			if j > 0:
				up = s_matrix[j-1][i][0]-eps
				mid = s_matrix[j-1][i][1]-sigma
				s_matrix[j][i][0] = max(up, mid)
				if s_matrix[j][i][0] == up:
					backtrack[(j, i, 0)] = (i,j-1,0)
				elif s_matrix[j][i][0] == mid:
					backtrack[(j, i, 0)] = (i,j-1,1)
			# upper(j,i) = max(upper(j,i-1)-eps,mid(j,i-1)-sigma)
			if i > 0:
				left = s_matrix[j][i-1][2]-eps
				mid = s_matrix[j][i-1][1]-sigma
				s_matrix[j][i][2] = max(left, mid)
				if s_matrix[j][i][2] == left:
					backtrack[(j, i, 2)] = (i-1,j,2)
				elif s_matrix[j][i][2] == mid:
					backtrack[(j, i, 2)] = (i-1,j,1)
			# mid(j,i) = max(mid(j-1,i-1)+match,lower(j,i),upper(j,i))
			if i > 0 or j > 0:
				lower = s_matrix[j][i][0]
				upper = s_matrix[j][i][2] 
				if j > 0 and i > 0:
					match = blosum_62[blosum_index.index(seq_v[i-1])][blosum_index.index(seq_w[j-1])]
					diagonal = s_matrix[j-1][i-1][1]+match
				else:
					diagonal = -float("inf")
				s_matrix[j][i][1] = max(lower, upper, diagonal)
				if s_matrix[j][i][1] == lower:
					backtrack[(j, i, 1)] = (i,j,0)
				elif s_matrix[j][i][1] == upper:
					backtrack[(j, i, 1)] = (i,j,2)
				elif s_matrix[j][i][1] == diagonal:
					backtrack[(j, i, 1)] = (i-1,j-1,1)	
			  
	for k in s_matrix:
		print k
		
	print s_matrix[-1][-1][1]
	return backtrack

def fit(backtrack, seq_v, seq_w, i, j, k):
	LCS_v = list()
	LCS_w = list()
	while i > 0 and j > 0:
		old_i, old_j, old_k = i, j, k
		i, j, k = backtrack[(j,i,k)]
		if k != 1 and old_k == 1:
			continue
		LCS_v.append("-" if old_i == i else seq_v[i])
		LCS_w.append("-" if old_j == j else seq_w[j])
	LCS_v.reverse()
	LCS_w.reverse()
	print "".join(LCS_v)
	print "".join(LCS_w)

backtrack = overlap_alignment(seq_v, seq_w)
fit(backtrack, seq_v, seq_w, len(seq_v), len(seq_w), 1)