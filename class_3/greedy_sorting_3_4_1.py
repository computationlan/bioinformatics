"""
Sample Input:
(-3 +4 +1 +5 -2)
Sample Output:
(-1 -4 +3 +5 -2)
(+1 -4 +3 +5 -2)
(+1 +2 -5 -3 +4)
(+1 +2 +3 +5 +4)
(+1 +2 +3 -4 -5)
(+1 +2 +3 +4 -5)
(+1 +2 +3 +4 +5)
"""
permutation = "-3 +4 +1 +5 -2".split()
data_list = []
for each in permutation:
	data_list.append(int(each))

def reversal(lst, n):
	result = lst[:n]
	idx = n
	middl = []
	while abs(lst[idx]) != n+1:
		middl.append(-lst[idx])
		idx+= 1
	middl.append(-lst[idx])
	middl.reverse()
	result += middl + lst[idx+1:]
	'''for each in middl:
		result.append(each)
	if idx < len(lst)-1:
		for each in lst[idx+1:]:
			result.append(each)'''
	return result
	
def greedy_sorting(p):
	len_p = len(p)
	approx_reversal_distance = 0
	result = []
	for k in range(1, len_p+1):
		if p[k-1] != k:
			approx_reversal_distance += 1
			p = reversal(p, k-1)
			result.append(p)
		if p[k-1] == -(k):
			approx_reversal_distance += 1
			p = reversal(p, k-1)
			result.append(p)
	print approx_reversal_distance
	return result
	
b = greedy_sorting(data_list)
for each in b:
	numbers = ["{:+}".format(k) for k in each]
	print '('+' '.join(numbers)+')'
