def recursive_change(money, coins):
	if money == 0:
		return 0
	min_num_coins = money
	for coin in coins:
		if money >= coin:
			num_coins = recursive_change(money - coin, coins)
			if num_coins + 1 < min_num_coins:
				min_num_coins = num_coins + 1
	return min_num_coins
	
for i in range(13, 23, 1):
	print recursive_change(i, [1, 4, 5]),