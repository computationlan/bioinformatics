"""
Sample Input:
2 2
2.31 4.55
5.96 9.08
--------
3.42 6.03
6.23 8.25
4.76 1.64
4.47 4.33
3.95 7.61
8.93 2.97
9.74 4.03
1.73 1.28
9.72 5.01
7.27 3.77

Sample Output:
18.246
"""
import math
raw_data = []
with open("dataset5.txt") as file:
	setting = file.readline().rstrip().split(" ")
	k = int(setting[0])
	m = int(setting[1])
	while True:
		each = file.readline().rstrip() 		
		if len(each) == 0:
			break
		raw_data.append(each)
centers = list()
data_points = list()		
for i in range(k):
	list1 = raw_data[i].split(" ")
	list2 = list()
	for j in list1:
		list2.append(float(j))
	centers.append(list2)
for n in range(k+1, len(raw_data)):
	list1 = raw_data[n].split(" ")
	list2 = list()
	for i in list1:
		list2.append(float(i))
	data_points.append(list2)

def error_distortion(centers, data_points, k, m):
		d_sum = 0
		for each in data_points:
			min_d = float("inf")
			for center in centers:
				c_sum = 0
				for c in range(m):
					c_sum += (center[c] - each[c])**2
				d = math.sqrt(c_sum)
				if d < min_d:
					min_d = d
			d_sum += min_d**2
		distortion = d_sum/len(data_points)
		return distortion
	
print error_distortion(centers, data_points, k, m)