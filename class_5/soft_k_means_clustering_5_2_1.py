"""
Sample Input:
2 2
2.7
1.3 1.1
1.3 0.2
0.6 2.8
3.0 3.2
1.2 0.7
1.4 1.6
1.2 1.0
1.2 1.1
0.6 1.5
1.8 2.6
1.2 1.3
1.2 1.0
0.0 1.9

Sample Output:
1.662 2.623
1.075 1.148
"""
import math
raw_data = []
with open("dataset7.txt") as file:
	setting = file.readline().rstrip().split(" ")
	k = int(setting[0])
	m = int(setting[1])
	beta = float(file.readline().rstrip())
	while True:
		each = file.readline().rstrip() 		
		if len(each) == 0:
			break
		raw_data.append(each)
data_list = list()		
for each in raw_data:
	list1 = each.split(" ")
	list2 = list()
	for i in list1:
		list2.append(float(i))
	data_list.append(tuple(list2))		
def soft_k_means_clustering(data_list, k, m, beta):
	centers = []
	for each_k in range(k):
		centers.append(data_list[each_k])
	for cycle in range(100):
		hidden_dict = dict() #####
		for each in data_list:
			point_sum = 0
			hidden_matrix = []
			for center in centers:
				c_sum = 0
				for c in range(m):
					c_sum += (center[c] - each[c])**2
				d = math.sqrt(c_sum)
				newton_d = math.e**(-beta*d)
				point_sum += newton_d
				hidden_matrix.append(newton_d)
			list1 = []
			for center in range(k):
				list1.append(hidden_matrix[center]/point_sum)
			hidden_dict[each] = list1
		new_centers = []
		for cntr in range(k):
			cntr_coord = []
			for i in range(m):
				sum_top = 0
				sum_bottom = 0
				for data_p in data_list:
					sum_top += hidden_dict[data_p][cntr]*data_p[i]
					sum_bottom += hidden_dict[data_p][cntr]
				cntr_coord.append(sum_top/sum_bottom)
			new_centers.append(tuple(cntr_coord))	
		centers = new_centers[:]
	return centers
result = soft_k_means_clustering(data_list, k, m, beta)
for i in result:
	print " ".join(str(x) for x in i)