"""
Sample Input:
3 2
0.0 0.0
5.0 5.0
0.0 5.0
1.0 1.0
2.0 2.0
3.0 3.0
1.0 2.0

Sample Output:
0.0 0.0
5.0 5.0
0.0 5.0
"""
import math
raw_data = []
with open("dataset4.txt") as file:
	setting = file.readline().rstrip().split(" ")
	k = int(setting[0])
	m = int(setting[1])
	while True:
		each = file.readline().rstrip() 		
		if len(each) == 0:
			break
		raw_data.append(each)
data_list = list()		
for each in raw_data:
	list1 = each.split(" ")
	list2 = list()
	for i in list1:
		list2.append(float(i))
	data_list.append(list2)		

def farthest_first_traversal(data_list, k, m):
	centers = [data_list[0]]
	while len(centers) < k:
		distance = 0
		point = []
		for each in data_list:
			min_d = float("inf")
			for center in centers:
				c_sum = 0
				for c in range(m):
					c_sum += (center[c] - each[c])**2
				d = math.sqrt(c_sum)
				if d < min_d:
					min_d = d
			if min_d > distance:
				distance = min_d 
				point = each
		centers.append(point)
	return centers
	
result = farthest_first_traversal(data_list, k, m)
for i in result:
	print " ".join(str(x) for x in i)