"""
Sample Input:
2 2
1.3 1.1
1.3 0.2
0.6 2.8
3.0 3.2
1.2 0.7
1.4 1.6
1.2 1.0
1.2 1.1
0.6 1.5
1.8 2.6
1.2 1.3
1.2 1.0
0.0 1.9

Sample Output:
1.800 2.867
1.060 1.140
"""
import math
raw_data = []
with open("dataset6.txt") as file:
	setting = file.readline().rstrip().split(" ")
	k = int(setting[0])
	m = int(setting[1])
	while True:
		each = file.readline().rstrip() 		
		if len(each) == 0:
			break
		raw_data.append(each)
data_list = list()		
for each in raw_data:
	list1 = each.split(" ")
	list2 = list()
	for i in list1:
		list2.append(float(i))
	data_list.append(tuple(list2))		

def lloyd(data_list, k, m):
	centers = []
	for each_k in range(k):
		centers.append(data_list[each_k])
	flag = True
	while flag:
		clusters = dict()
		for each in data_list:
			min_d = float("inf")
			for center in centers:
				c_sum = 0
				for c in range(m):
					c_sum += (center[c] - each[c])**2
				d = math.sqrt(c_sum)
				if d < min_d:
					min_d = d
					cntr = center
			if cntr not in clusters:
				clusters[cntr] = [each]
			else:
				clusters[cntr].append(each)
		new_centers = []
		for cntr in clusters:
			new_coord = []
			for i in range(m):
				count = 0
				for c in clusters[cntr]:
					count += c[i]
				new_coord.append(float(count)/len(clusters[cntr]))
			new_centers.append(tuple(new_coord))
		if new_centers == centers:
			flag = False
		else:
			centers = new_centers
		
	return centers
	
result = lloyd(data_list, k, m)

for i in result:
	print " ".join(str(x) for x in i)