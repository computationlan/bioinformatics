"""
Sample Input:
7
0.00 0.74 0.85 0.54 0.83 0.92 0.89
0.74 0.00 1.59 1.35 1.20 1.48 1.55
0.85 1.59 0.00 0.63 1.13 0.69 0.73
0.54 1.35 0.63 0.00 0.66 0.43 0.88
0.83 1.20 1.13 0.66 0.00 0.72 0.55
0.92 1.48 0.69 0.43 0.72 0.00 0.80
0.89 1.55 0.73 0.88 0.55 0.80 0.00

Sample Output:
4 6
5 7
3 4 6
1 2
5 7 3 4 6
1 2 5 7 3 4 6
"""
raw_data = []
with open("dataset9.txt") as file:
	n_nodes = int(file.readline())
	while True:
		each = file.readline().rstrip() 		
		if len(each) == 0:
			break
		raw_data.append(each)
data_list = list()		
for each in raw_data:
	list1 = each.split(" ")
	list2 = list()
	for i in list1:
		list2.append(float(i))
	data_list.append(list2)		

def upgma(data_list, n_nodes):
	clusters = [n for n in range(1, n_nodes+1)]
	weight = [1 for n in range(1, n_nodes+1)]
	result = []
	while len(clusters) > 1:
		min_c = float("inf")
		for i in range(len(data_list)):
			for j in range(len(data_list[i])):
				if i != j and data_list[i][j] < min_c:
					min_c = data_list[i][j]
					cluster1, cluster2 = i, j
		new_weight = weight[cluster1] + weight[cluster2]
		result.append((clusters[cluster1], clusters[cluster2]))
		new_row = []
		for i in range(len(data_list[0])):
			if i != cluster1 and i != cluster2:
				#new_row.append(min(data_list[cluster1][i], data_list[cluster2][i]))
				new_row.append((data_list[cluster1][i]*weight[cluster1] + data_list[cluster2][i]*weight[cluster2])/new_weight)
			elif i == cluster1:
				new_row.append(0)
		data_list.pop(cluster2)
		for i in range(len(data_list[0])-1):
			data_list[i][cluster1] = new_row[i]
			data_list[i].pop(cluster2) 
		weight[cluster1] = new_weight
		weight.pop(cluster2)
		data_list[cluster1] = new_row
		clusters[cluster1] = str(clusters[cluster1])+" "+str(clusters[cluster2])
		clusters.pop(cluster2)

	return result

result = upgma(data_list, n_nodes)
for each in result:
	print each[0], each[1]