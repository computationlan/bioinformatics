	
def linear_spectrum(peptide):
	prefix_mass = [0]
	for i in range(len(peptide)):
		prefix_mass.append(peptide[i]+prefix_mass[-1])
	spectrum_result = [0]
	for i in range(len(peptide)):
		for j in range(i+1, len(peptide)+1):
			spectrum_result.append(prefix_mass[j]-prefix_mass[i])
	return sorted(spectrum_result)	

def cyclic_spectrum(peptide):
	prefix_mass = [0]
	for i in range(len(peptide)):
		prefix_mass.append(peptide[i]+prefix_mass[-1])
	peptide_mass = prefix_mass[-1]
	cyclic_spectrum = [0]
	for i in range(len(peptide)):
		for j in range(i+1, len(peptide)+1):
			cyclic_spectrum.append(prefix_mass[j]-prefix_mass[i])
			if i > 0 and j < len(peptide):
				cyclic_spectrum.append(peptide_mass - (prefix_mass[j] - prefix_mass[i]))
	return sorted(cyclic_spectrum)
	
filename = "dataset14.txt"

def convolution(spectrum1):
	spectrum1.sort()
	result = []		
	copy_spec = spectrum1
	next_mass = copy_spec.pop()
	while len(copy_spec) > 0:
		for i in copy_spec:
			result.append(next_mass - i)
		next_mass = copy_spec.pop() 
	return_result = dict()
	for each in result:
		if each not in return_result:
			return_result[each] = 1
		else: 
			return_result[each] += 1	
	return return_result

spectrum = []
with open(filename) as fh:
	seq = fh.readline().rstrip().split()
	for i in seq:
		spectrum.append(int(i))	
spectrum.sort()
m = 20
n = 1000
spectrum2 = list(spectrum)
def alph(spectrum2, m):
	conv = convolution(spectrum2)
	result = []
	counter = 0
	conv_list = [x for x in conv]
	for each in conv_list:
		if each < 57 or each > 200:
			conv.pop(each)
	best_scores = sorted([x for x in conv.values()], reverse = True)
	scr_cutoff = 0
	if len(best_scores) < m:
		scr_cutoff = best_scores[-1]
	else: scr_cutoff = best_scores[m]
	for each in conv:
		if conv[each] >= scr_cutoff:
			result.append(each)
			counter += 1	
	return result

def score(peptide, spectrum, cyclic=False):
	theoretical_spec = cyclic_spectrum(peptide) if cyclic else linear_spectrum(peptide)
	score = 0
	score_dict = dict()
	for aa in spectrum:
		if aa in theoretical_spec:
			if aa not in score_dict:
				score_dict[aa] = 1
			else:
				score_dict[aa] +=1
			theoretical_spec.remove(aa)
	for each in score_dict.values():
		score += each
	return score
	
def trim(leaderboard, spectrum, n):
	linear_scores = dict()
	for j in leaderboard:
		linear_scores[j] = score(j, spectrum)
	decreasing_scores = sorted([i for i in linear_scores.values()], reverse = True)

	if len(decreasing_scores) < n:
		return leaderboard
	else:
		current_scr = decreasing_scores[n]
			
	return_list = []
	for each in linear_scores:
		if linear_scores[each] >= current_scr:
			return_list.append(each)
	return return_list
	
def convolution_cyclopeptide_sequencing(spectrum, m):
	peptides = set([()])
	parent_mass = spectrum[-1]
	best_score = 0
	aa_alph = alph(spectrum2, m)
	all_leaders = set()
	while len(peptides) > 0:
		new_peptides = set()
		for peptide in peptides:
			for i in aa_alph:
				new_peptides.add(peptide+(i,))
		peptides = new_peptides
		
		remove_elements = set()
		for each in peptides:
			if sum(each) == parent_mass:
				new_score = score(each, spectrum, True)
				if new_score > best_score:
					best_score = new_score
					all_leaders = set()
				if new_score == best_score:
					all_leaders.add(each)
			elif sum(each) > parent_mass:
				remove_elements.add(each)
		peptides.difference_update(remove_elements)
		peptides = trim(peptides, spectrum, n)
	return all_leaders

otvet = convolution_cyclopeptide_sequencing(spectrum, m)
print score(list(otvet)[0], spectrum, True)
for each in otvet:
	print "-".join(str(x) for x in each),	
