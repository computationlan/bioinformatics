import itertools
number = 4
dnas = ["".join(seq) for seq in itertools.product("01", repeat = number)]
print dnas
	
mers_list = []
graph = dict()
d = len(dnas[0]) - 1
for each in dnas:
	if each[0:d] not in mers_list:
		mers_list.append(each[0:d])
mers_list.sort()
for i in mers_list:
	if i not in graph:
		graph[i] = list()
for m in graph:
	for j in dnas:
		if m == j[:d]:
			graph[m].append(j[1:])	

def EulerianCycle(graph):
	result = list()
	cycle = list()
	new_graph = graph
	unused_nodes = set(graph.keys())
	while len(unused_nodes) > 0:
		element = unused_nodes.pop()
		unused_nodes.add(element)
		cycle.append(element)
		element_1 = element
		while len(new_graph[element_1]) > 0:
			new_node = new_graph[element_1].pop()
			cycle.append(new_node)
			if len(new_graph[element_1]) == 0:
				new_graph.pop(element_1)
				unused_nodes.discard(element_1)
			element_1 = new_node
			if element_1 not in new_graph:
				while element_1 not in new_graph:
					cycle_re = list()
					if cycle[-1] != cycle[0]:
						cycle_re.append(cycle[-1])
						for n in range(len(cycle)-1):
							cycle_re.append(cycle[n])
					else:
						cycle_re = cycle[:-1]
					cycle = cycle_re
					element_1 = cycle[-1]
					if len(unused_nodes) == 0:
						result = cycle.append(cycle[0])
						break
			if len(unused_nodes) == 0:		
				break
			
		if element in new_graph and len(new_graph[element]) == 0:
			new_graph.pop(element)
			unused_nodes.discard(element)
	return cycle	
result = EulerianCycle(graph)
#print '->'.join(result)
print_out = list()
print_out.append(result[0][:-1])
for each in result:
	print_out.append(each[-1])
print ''.join(print_out)
