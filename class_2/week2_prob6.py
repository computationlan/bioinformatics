"""
ATG
ATG
TGT
TGG
CAT
GGA
GAT
AGA
Sample Output:
AGA
ATG
ATG
CAT
GAT
TGGA
TGT
"""
dnas = list()
filename = "dataset6.txt"
with open(filename) as fh:
	while True:
		seq = fh.readline().rstrip() 		
		if len(seq) == 0:
			break
		dnas.append(seq)
graph = dict()
k = len(dnas[0])
for i in dnas:
	if i[:-1] not in graph:
		graph[i[:-1]] = list()
	if i[1:] not in graph:
		graph[i[1:]] = list()
	graph[i[:-1]].append(i[1:])
			
def compute_in_degrees(digraph):
	"""
	Compute in_degrees
	"""
	result = dict()
	for each in digraph:
		result[each] = 0
	for node in digraph.values():
		for each in node:
			result[each] += 1
	return result

in_degree = compute_in_degrees(graph)
out_degree = dict()
for each in in_degree:
	out_degree[each] = len(graph[each])
paths = list()
def maximal_non_branching_paths(graph):
	unused_nodes = set(graph.keys())
	for each_node in graph:
		if out_degree[each_node] != 1 or in_degree[each_node] != 1:
			if out_degree[each_node] > 0:
				for out_edge in graph[each_node]:
					non_branching_path = [each_node, out_edge]
					while out_degree[non_branching_path[-1]] == 1 and in_degree[non_branching_path[-1]] == 1:
						non_branching_path.append(graph[non_branching_path[-1]][0])
					paths.append(non_branching_path)
		elif out_degree[each_node] == 1 and in_degree[each_node] == 1:
			cycle = [each_node, graph[each_node][0]]
			while out_degree[cycle[-1]] == 1 and in_degree[cycle[-1]] == 1 and graph[cycle[-1]][0] in unused_nodes and graph[cycle[-1]][0] in unused_nodes:
				unused_nodes.discard(cycle[-1])
				cycle.append(graph[cycle[-1]][0])
				if cycle[0] == cycle[-1]:
					paths.append(cycle)
					break 
				
	return paths
	
result_list =  maximal_non_branching_paths(graph)
for each in result_list:
	print_out = each[0]
	for i in range(1, len(each)):
		print_out += each[i][-1]
	print print_out