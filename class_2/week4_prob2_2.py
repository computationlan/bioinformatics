aa = {"G": 57, "A": 71, "S": 87, "P": 97, "V": 99, "T": 101, "C": 103, "I": 113, "L": 113, "N": 114, "D": 115, "K": 128, "Q": 128, "E": 129, "M": 131, "H": 137, "F": 147, "R": 156, "Y": 163, "W": 186}
def linear_spectrum(peptide):
	prefix_mass = [0]
	for i in range(len(peptide)):
		prefix_mass.append(aa[peptide[i]]+prefix_mass[-1])
	linear_spectrum_result = [0]
	for i in range(len(peptide)):
		for j in range(i+1, len(peptide)+1):
			linear_spectrum_result.append(prefix_mass[j]-prefix_mass[i])
	return sorted(linear_spectrum_result)

def cyclic_spectrum(peptide):
	prefix_mass = [0]
	for i in range(len(peptide)):
		prefix_mass.append(aa[peptide[i]]+prefix_mass[-1])
	peptide_mass = prefix_mass[-1]
	cyclic_spectrum = [0]
	for i in range(len(peptide)):
		for j in range(i+1, len(peptide)+1):
			cyclic_spectrum.append(prefix_mass[j]-prefix_mass[i])
			if i > 0 and j < len(peptide):
				cyclic_spectrum.append(peptide_mass - (prefix_mass[j] - prefix_mass[i]))
	return sorted(cyclic_spectrum)
	
filename = "dataset11.txt"
filename2 = "dataset10.txt"
spectrum = []
with open(filename) as fh:
	seq = fh.readline().rstrip().split()
	for i in seq:
		spectrum.append(int(i))	

leaderboard = []
with open(filename2) as fh:
	seq = fh.readline().rstrip().split()
	for i in seq:
		leaderboard.append(i)	
		
n = 2

def score(peptide, spectrum):
	theoretical_spec = linear_spectrum(peptide)
	score = 0
	score_dict = dict()
	for aa in spectrum:
		if aa in theoretical_spec:
			if aa not in score_dict:
				score_dict[aa] = 1
			else:
				score_dict[aa] +=1
			theoretical_spec.remove(aa)
	for each in score_dict.values():
		score += each
	return score
	
def trim(leaderboard, spectrum, n):
	linear_scores = dict()
	for j in leaderboard:
		linear_scores[j] = score(j, spectrum)
	decreasing_scores = sorted([i for i in linear_scores.values()], reverse = True)
	quant_pep = 0
	for i in range(len(decreasing_scores)):
		if quant_pep < n:
			quant_pep += 1
			current_scr = decreasing_scores[i]
		elif quant_pep >= n and decreasing_scores[i] == current_scr:
			quant_pep += 1
		else: 
			break 		
	return_list = []
	for each in linear_scores:
		if linear_scores[each] >= current_scr:
			return_list.append(each)
	return return_list

result = trim(leaderboard, spectrum, n)
for each in result:
	print each,
