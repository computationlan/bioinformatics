aa = {"G": 57, "A": 71, "S": 87, "P": 97, "V": 99, "T": 101, "C": 103, "I": 113, "L": 113, "N": 114, "D": 115, "K": 128, "Q": 128, "E": 129, "M": 131, "H": 137, "F": 147, "R": 156, "Y": 163, "W": 186}
def linear_spectrum(peptide):
	prefix_mass = [0]
	for i in range(len(peptide)):
		prefix_mass.append(aa[peptide[i]]+prefix_mass[-1])
	linear_spectrum_result = [0]
	for i in range(len(peptide)):
		for j in range(i+1, len(peptide)+1):
			linear_spectrum_result.append(prefix_mass[j]-prefix_mass[i])
	return sorted(linear_spectrum_result)
	
#print linear_spectrum("NQEL")

def cyclic_spectrum(peptide):
	prefix_mass = [0]
	for i in range(len(peptide)):
		prefix_mass.append(aa[peptide[i]]+prefix_mass[-1])
	peptide_mass = prefix_mass[-1]
	cyclic_spectrum = [0]
	for i in range(len(peptide)):
		for j in range(i+1, len(peptide)+1):
			cyclic_spectrum.append(prefix_mass[j]-prefix_mass[i])
			if i > 0 and j < len(peptide):
				cyclic_spectrum.append(peptide_mass - (prefix_mass[j] - prefix_mass[i]))
	return sorted(cyclic_spectrum)
	
for each in cyclic_spectrum("NQEL"):
	print each, 
	
	