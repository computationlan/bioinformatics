filename = "dataset13.txt"

spectrum = []
with open(filename) as fh:
	seq = fh.readline().rstrip().split()
	for i in seq:
		spectrum.append(int(i))	
spectrum.sort()
print spectrum		
result = []		
copy_spec = spectrum
next_mass = copy_spec.pop()
while len(copy_spec) > 0:
	for i in copy_spec:
		result.append(next_mass - i)
	next_mass = copy_spec.pop() 
	
for each in result:
	if each > 0:
		print each,