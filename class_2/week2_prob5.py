"""
Sample Input:
4 2
GACC|GCGC
ACCG|CGCC
CCGA|GCCG
CGAG|CCGG
GAGC|CGGA
Sample Output:
GACCGAGCGCCGGA
"""
dnas = list()
filename = "dataset5.txt"
with open(filename) as fh:
	while True:
		seq = fh.readline().rstrip() 		
		if len(seq) == 0:
			break
		dnas.append(seq)
dnas2 = list()		
for i in dnas:
	dnas2.append(i.split())
k = int(dnas2[0][0])
d = int(dnas2[0][1])
dnas2.pop(0)
patterns = list()
for i in dnas2:
	patterns.append(tuple(i[0].split('|')))
print patterns	

graph = dict()
for each in patterns:
	new_1 = each[0][0:(k-1)]
	new_2 = each[1][0:(k-1)]
	if (new_1, new_2) not in graph:
		graph[(new_1, new_2)] = set()
	if (each[0][1:], each[1][1:]) not in graph:
		graph[(each[0][1:], each[1][1:])] = set()
	graph[(new_1, new_2)].add((each[0][1:], each[1][1:]))	
print graph

def EulerianPath(graph):
	result = list()
	cycle = list()
	new_graph = graph
	unused_nodes = set(graph.keys())
	while len(unused_nodes) > 0:
		element = unused_nodes.pop()
		unused_nodes.add(element)
		cycle.append(element)
		element_1 = element
		while len(new_graph[element_1]) > 0:
			new_node = new_graph[element_1].pop()
			cycle.append(new_node)
			if len(new_graph[element_1]) == 0:
				new_graph.pop(element_1)
				unused_nodes.discard(element_1)
			element_1 = new_node
			if element_1 not in new_graph:
				while element_1 not in new_graph:
					cycle_re = list()
					if cycle[-1] != cycle[0]:
						cycle_re.append(cycle[-1])
						for n in range(len(cycle)-1):
							cycle_re.append(cycle[n])
					else:
						cycle_re = cycle[:-1]
					cycle = cycle_re
					element_1 = cycle[-1]
					if len(unused_nodes) == 0:
						result = cycle.append(cycle[0])
						break
			if len(unused_nodes) == 0:		
				break
			
		if element in new_graph and len(new_graph[element]) == 0:
			new_graph.pop(element)
			unused_nodes.discard(element)
	return cycle	

def compute_in_degrees(digraph):
    """
    Compute in_degrees
    """
    result = dict()
    for each in digraph:
        result[each] = 0
    for node in digraph:
        for node_2 in digraph:
            if node != node_2 and node in digraph[node_2]:
                result[node] += 1
    return result

in_degree = compute_in_degrees(graph)
for each in in_degree:
	out_degree = len(graph[each])
	if in_degree[each] > out_degree:
		tail = each
	elif in_degree[each] < out_degree:
		head = each

graph[tail].add(head)
result = EulerianPath(graph)
for pos in range(len(result)-1):
	if result[pos] == tail and result[pos+1] == head:
		result = result[pos+1:] + result[1:pos+1]
		break
		
print result
def string_spelled_by_patterns(list_1):
	result = list_1[0]
	for i in range(len(list_1)-1):
		result += list_1[i+1][-1]
	return result
def StringSpelledByGappedPatterns(patterns, k, d):
	first_patterns = list()
	second_patterns = list()
	for each in range(len(patterns)):
		first_patterns.append(patterns[each][0])
		second_patterns.append(patterns[each][1])
	prefix_string = string_spelled_by_patterns(first_patterns)
	suffix_string = string_spelled_by_patterns(second_patterns)
	for i in range((k+d+1), len(prefix_string)):
		if prefix_string[i] != suffix_string[i-k-d]:
			return "there is no string spelled by the gapped patterns"
	return prefix_string+suffix_string[(len(prefix_string)-k-d):]		
print StringSpelledByGappedPatterns(result, k-1, d+1)