aa = {"G": 57, "A": 71, "S": 87, "P": 97, "V": 99, "T": 101, "C": 103, "I": 113, "L": 113, "N": 114, "D": 115, "K": 128, "Q": 128, "E": 129, "M": 131, "H": 137, "F": 147, "R": 156, "Y": 163, "W": 186}
aa_alph = [57, 71, 87, 97, 99, 101, 103, 113, 113, 114, 115, 128, 128, 129, 131, 137, 147, 156, 163, 186]
filename = "dataset7.txt"
spectrum = []
with open(filename) as fh:
	seq = fh.readline().rstrip().split()
	for i in seq:
		spectrum.append(int(i))

#spectrum = [0, 113, 128, 186, 241, 299, 314, 427]
#output = 186-128-113 186-113-128 128-186-113 128-113-186 113-186-128 113-128-186

def spectrum_peptide(peptide):
	prefix_mass = [0]
	for i in range(len(peptide)):
		prefix_mass.append(peptide[i]+prefix_mass[-1])
	peptide_mass = prefix_mass[-1]	
	spectrum_result = [0]
	for i in range(len(peptide)):
		for j in range(i+1, len(peptide)+1):
			spectrum_result.append(prefix_mass[j]-prefix_mass[i])
			if i > 0 and j < len(peptide):
				spectrum_result.append(peptide_mass - (prefix_mass[j] - prefix_mass[i]))
	return sorted(spectrum_result)

def linear_spectrum(peptide):
	prefix_mass = [0]
	for i in range(len(peptide)):
		prefix_mass.append(peptide[i]+prefix_mass[-1])
	spectrum_result = [0]
	for i in range(len(peptide)):
		for j in range(i+1, len(peptide)+1):
			spectrum_result.append(prefix_mass[j]-prefix_mass[i])
	return sorted(spectrum_result)

#print spectrum_peptide((186, 113, 128))

def consistent(peptide, spectrum):
	spec = linear_spectrum(peptide)
	copy = list(spectrum)
	for each in spec:
		if each in copy:
			copy.remove(each)
		else:
			return False
	return True

def cyclopeptide_sequencing(spectrum):
	aa1 = []
	for a in spectrum:
		if a > 0 and a <= 186:
			aa1.append(a)
	peptides = set([()])
	parent_mass = spectrum[-1]
	while len(peptides) > 0:
		new_peptides = set()
		for peptide in peptides:
			for i in aa1:
				new_peptides.add(peptide+(i,))
		peptides = new_peptides
		
		remove_elements = set()
		for each in peptides:
			if sum(each) == parent_mass:
				if spectrum_peptide(each) == spectrum:
					yield each
				remove_elements.add(each)
			elif not consistent(each, spectrum):
				remove_elements.add(each)
		peptides.difference_update(remove_elements)

otvet = cyclopeptide_sequencing(spectrum)
for each in otvet:
	print "-".join(str(x) for x in each)