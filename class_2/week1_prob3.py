"""
Sample Input:
ATGCG
GCATG
CATGC
AGGCA
GGCAT
Sample Output:
CATGC -> ATGCG
GCATG -> CATGC
GGCAT -> GCATG
AGGCA -> GGCAT
"""

dnas = []
filename = "dataset3.txt"
with open(filename) as fh:
	while True:
		seq = fh.readline().rstrip() 		
		if len(seq) == 0:
			break
		dnas.append(seq)	
dnas_set = set(dnas)
for i in dnas_set:
    for j in dnas_set:
        if i[1:] == j[:-1]:
            print i + " -> " + j


