"""
1 -> 2
2 -> 3
3 -> 4,5
6 -> 7
7 -> 6
Sample Output:
1 -> 2 -> 3
3 -> 4
3 -> 5
7 -> 6 -> 7
"""
graph = dict()
dnas = list()
filename = "dataset6_1.txt"
with open(filename) as fh:
	while True:
		seq = fh.readline().rstrip() 		
		if len(seq) == 0:
			break
		dnas.append(seq)
dnas2 = list()		
for i in dnas:
	dnas2.append(i.split())
for i in dnas2:
	list_1 = i[2].split(',')
	graph[i[0]] = set([])
	for j in list_1:
		graph[i[0]].add(j)
for i in graph.values():
	for j in i:
		if j not in graph:
			graph[j] = set()		
def compute_in_degrees(digraph):
    """
    Compute in_degrees
	"""
    result = dict()
    for each in digraph:
        result[each] = 0
    for node in digraph:
        for node_2 in digraph:
            if node != node_2 and node in digraph[node_2]:
                result[node] += 1
    return result

in_degree = compute_in_degrees(graph)
out_degree = dict()
for each in in_degree:
	out_degree[each] = len(graph[each])
paths = list()
def maximal_non_branching_paths(graph):
	unused_nodes = set(graph.keys())
	for each_node in graph:
		if out_degree[each_node] != 1 or in_degree[each_node] != 1:
			if out_degree[each_node] > 0:
				for out_edge in graph[each_node]:
					non_branching_path = [each_node, out_edge]
					while out_degree[non_branching_path[-1]] == 1 and in_degree[non_branching_path[-1]] == 1:
						non_branching_path.append(list(graph[non_branching_path[-1]])[0])
					paths.append(non_branching_path)
		elif out_degree[each_node] == 1 and in_degree[each_node] == 1:
			cycle = [each_node, list(graph[each_node])[0]]
			while out_degree[cycle[-1]] == 1 and in_degree[cycle[-1]] == 1 and list(graph[cycle[-1]])[0] in unused_nodes and list(graph[cycle[-1]])[0] in unused_nodes:
				unused_nodes.discard(cycle[-1])
				cycle.append(list(graph[cycle[-1]])[0])
				if cycle[0] == cycle[-1]:
					paths.append(cycle)
					break 
				
	return paths
	
result_list =  maximal_non_branching_paths(graph)
for each in result_list:
	print ' -> '.join(each)