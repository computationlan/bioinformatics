"""
Sample Input:
4
AAGATTCTCTAAGA
Sample Output:
AAG -> AGA,AGA
AGA -> GAT
ATT -> TTC
CTA -> TAA
CTC -> TCT
GAT -> ATT
TAA -> AAG
TCT -> CTA,CTC
TTC -> TCT
defaultdict
"""
integer_k = 4
text_str = "AAGATTCTCTAAGA"	
result = dict()
for i in range(len(text_str)-integer_k+1):
	result[text_str[i:(i+integer_k-1)]] = list()
for j in range(len(text_str)-integer_k+1):
	result[text_str[j:(j+integer_k-1)]].append(text_str[(j+1):(j+integer_k)])	
for k in result:
	str_ans = ""
	for n in range(len(result[k])):
		str_ans += result[k][n]
		if len(result[k]) - n > 1:
			str_ans += ","
	print k +" -> "+ str_ans
	 



