aa = {"G": 57, "A": 71, "S": 87, "P": 97, "V": 99, "T": 101, "C": 103, "I": 113, "L": 113, "N": 114, "D": 115, "K": 128, "Q": 128, "E": 129, "M": 131, "H": 137, "F": 147, "R": 156, "Y": 163, "W": 186}
aa_alph = [57, 71, 87, 97, 99, 101, 103, 113, 113, 114, 115, 128, 128, 129, 131, 137, 147, 156, 163, 186]

	
def linear_spectrum(peptide):
	prefix_mass = [0]
	for i in range(len(peptide)):
		prefix_mass.append(peptide[i]+prefix_mass[-1])
	spectrum_result = [0]
	for i in range(len(peptide)):
		for j in range(i+1, len(peptide)+1):
			spectrum_result.append(prefix_mass[j]-prefix_mass[i])
	return sorted(spectrum_result)	

def cyclic_spectrum(peptide):
	prefix_mass = [0]
	for i in range(len(peptide)):
		prefix_mass.append(peptide[i]+prefix_mass[-1])
	peptide_mass = prefix_mass[-1]
	cyclic_spectrum = [0]
	for i in range(len(peptide)):
		for j in range(i+1, len(peptide)+1):
			cyclic_spectrum.append(prefix_mass[j]-prefix_mass[i])
			if i > 0 and j < len(peptide):
				cyclic_spectrum.append(peptide_mass - (prefix_mass[j] - prefix_mass[i]))
	return sorted(cyclic_spectrum)
	
filename = "dataset12.txt"

spectrum = []
with open(filename) as fh:
	seq = fh.readline().rstrip().split()
	for i in seq:
		spectrum.append(int(i))	
		
n = 1000

def score(peptide, spectrum, cyclic=False):
	theoretical_spec = cyclic_spectrum(peptide) if cyclic else linear_spectrum(peptide)
	score = 0
	score_dict = dict()
	for aa in spectrum:
		if aa in theoretical_spec:
			if aa not in score_dict:
				score_dict[aa] = 1
			else:
				score_dict[aa] +=1
			theoretical_spec.remove(aa)
	for each in score_dict.values():
		score += each
	return score
	
def trim(leaderboard, spectrum, n):
	linear_scores = dict()
	for j in leaderboard:
		linear_scores[j] = score(j, spectrum)
	decreasing_scores = sorted([i for i in linear_scores.values()], reverse = True)

	if len(decreasing_scores) < n:
		return leaderboard
	else:
		current_scr = decreasing_scores[n]
			
	return_list = []
	for each in linear_scores:
		if linear_scores[each] >= current_scr:
			return_list.append(each)
	return return_list

def leaderboard_cyclopeptide_sequencing(spectrum):
	peptides = set([()])
	parent_mass = spectrum[-1]
	best_score = 0
	all_leaders = set()
	while len(peptides) > 0:
		new_peptides = set()
		for peptide in peptides:
			for i in aa_alph:
				new_peptides.add(peptide+(i,))
		peptides = new_peptides
		
		remove_elements = set()
		for each in peptides:
			if sum(each) == parent_mass:
				new_score = score(each, spectrum, True)
				if new_score > best_score:
					best_score = new_score
					all_leaders = set()
				if new_score == best_score:
					all_leaders.add(each)
			elif sum(each) > parent_mass:
				remove_elements.add(each)
		peptides.difference_update(remove_elements)
		peptides = trim(peptides, spectrum, n)
	return all_leaders

otvet = leaderboard_cyclopeptide_sequencing(spectrum)
print score(list(otvet)[0], spectrum, True)
for each in otvet:
	print "-".join(str(x) for x in each),

