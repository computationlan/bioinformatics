"""
Sample Input:
4 2
GACC|GCGC
ACCG|CGCC
CCGA|GCCG
CGAG|CCGG
GAGC|CGGA
Sample Output:
GACCGAGCGCCGGA
"""
dnas = list()
filename = "dataset5_1.txt"
with open(filename) as fh:
	while True:
		seq = fh.readline().rstrip() 		
		if len(seq) == 0:
			break
		dnas.append(seq)
dnas2 = list()		
for i in dnas:
	dnas2.append(i.split())
k = int(dnas2[0][0])
d = int(dnas2[0][1])
dnas2.pop(0)
patterns = list()
for i in dnas2:
	patterns.append(tuple(i[0].split('|')))
def string_spelled_by_patterns(list_1):
	result = list_1[0]
	for i in range(len(list_1)-1):
		result += list_1[i+1][-1]
	return result
def StringSpelledByGappedPatterns(patterns, k, d):
	first_patterns = list()
	second_patterns = list()
	for each in range(len(patterns)):
		first_patterns.append(patterns[each][0])
		second_patterns.append(patterns[each][1])
	prefix_string = string_spelled_by_patterns(first_patterns)
	suffix_string = string_spelled_by_patterns(second_patterns)
	for i in range((k+d+1), len(prefix_string)):
		if prefix_string[i] != suffix_string[i-k-d]:
			return "there is no string spelled by the gapped patterns"
	return prefix_string+suffix_string[(len(prefix_string)-k-d):]		
	

print StringSpelledByGappedPatterns(patterns, k, d)