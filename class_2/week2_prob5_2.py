dnas = list()
filename = "dataset5.txt"
with open(filename) as fh:
	while True:
		seq = fh.readline().rstrip() 		
		if len(seq) == 0:
			break
		dnas.append(seq)
dnas2 = list()		
for i in dnas:
	dnas2.append(i.split())
k = int(dnas2[0][0])
d = int(dnas2[0][1])
dnas2.pop(0)
patterns = list()
for i in dnas2:
	patterns.append(tuple(i[0].split('|')))
first_patterns = list()
second_patterns = list()
for each in range(len(patterns)):
	first_patterns.append(patterns[each][0])
	second_patterns.append(patterns[each][1])
	
def make_graph(list_1):
	mers_list = []
	graph = dict()
	d2 = len(list_1[0]) - 1
	for each in list_1:
		if each[0:d2] not in mers_list:
			mers_list.append(each[0:d2])
	mers_list.sort()
	for i in mers_list:
		if i not in graph:
			graph[i] = list()
	for m in graph:
		for j in list_1:
			if m == j[:d2]:
				graph[m].append(j[1:])	
	return graph
	
def EulerianCycle(graph):
	result = list()
	cycle = list()
	new_graph = graph
	unused_nodes = set(graph.keys())
	while len(unused_nodes) > 0:
		element = unused_nodes.pop()
		unused_nodes.add(element)
		cycle.append(element)
		element_1 = element
		print element_1
		while len(new_graph[element_1]) > 0:
			new_node = new_graph[element_1].pop()
			cycle.append(new_node)
			if len(new_graph[element_1]) == 0:
				new_graph.pop(element_1)
				unused_nodes.discard(element_1)
			element_1 = new_node
			if element_1 not in new_graph:
				while element_1 not in new_graph:
					cycle_re = list()
					if cycle[-1] != cycle[0]:
						cycle_re.append(cycle[-1])
						for n in range(len(cycle)-1):
							cycle_re.append(cycle[n])
					else:
						cycle_re = cycle[:-1]
					cycle = cycle_re
					element_1 = cycle[-1]
					if len(unused_nodes) == 0:
						result = cycle.append(cycle[0])
						break
			if len(unused_nodes) == 0:		
				break
			
		if element in new_graph and len(new_graph[element]) == 0:
			new_graph.pop(element)
			unused_nodes.discard(element)
	return cycle
		
first_graph = make_graph(first_patterns)
second_graph = make_graph(second_patterns)
print first_graph, second_graph	
#first_result = EulerianCycle(first_graph)
#print first_result
"""result = EulerianCycle(graph)
print_out = list()
print_out.append(result[0][:-1])
for each in result:
	print_out.append(each[-1])
print ''.join(print_out)"""
