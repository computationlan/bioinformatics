"""
Sample Input:
0 -> 2
1 -> 3
2 -> 1
3 -> 0,4
6 -> 3,7
7 -> 8
8 -> 9
9 -> 6
Sample Output:
6->7->8->9->6->3->0->2->1->3->4
"""
import random

graph = dict()
dnas = list()
filename = "dataset2.txt"
with open(filename) as fh:
	while True:
		seq = fh.readline().rstrip() 		
		if len(seq) == 0:
			break
		dnas.append(seq)
dnas2 = list()		
for i in dnas:
	dnas2.append(i.split())
for i in dnas2:
	list_1 = i[2].split(',')
	graph[i[0]] = set([])
	for j in list_1:
		graph[i[0]].add(j)
def EulerianPath(graph):
	result = list()
	#return_result = list()
	new_graph = graph
	unused_nodes = set(graph.keys())
	while len(unused_nodes) > 0:
		cycle = list()
		element = unused_nodes.pop()
		unused_nodes.add(element)
		cycle.append(element)
		element_1 = element
		while element_1 in new_graph and len(new_graph[element_1]) > 0:
			new_node = new_graph[element_1].pop()
			cycle.append(new_node)
			if len(new_graph[element_1]) == 0:
				new_graph.pop(element_1)
				unused_nodes.discard(element_1)
			element_1 = new_node
			if element_1 not in new_graph and cycle[0] == cycle[-1]:
				b_cycle = cycle
				b_element_1 = element_1
				for i in range(len(cycle)):
					cycle_re = cycle[1:]
					cycle_re.append(cycle[1])
					cycle = cycle_re
					element_1 = cycle[-1]
					if element_1 in new_graph:
						break
				if element_1 not in new_graph:
					cycle = b_cycle
					element_1 = b_element_1
						
		result.append(cycle)
	result_2 = list()	
	#print len(result)
	while len(result) > 1:	
		k = random.randrange(0, len(result))
		m = random.randrange(0, len(result))
		print k,m, len(result)
		if k != m and result[k][0] == result[m][-1]:
			result_2.append(result[m]+result[k][1:])
			if m>k:
				result.pop(m)
				result.pop(k)
				result.append(result_2)
				result_2 = list()
			else:
				result.pop(k)
				result.pop(m)
				result.append(result_2)
				result_2 = list()
	if len(result) > 0 and result[0][0] == result_2[0][-1]:
		return_result = result_2[0]+result[0][1:]
	elif len(result) > 0:
		return_result = result[0]+ result_2[0][1:]
	
	return return_result
result_3 = EulerianPath(graph)
print result_3
#print '->'.join(result_3)