"""
Sample Input:
0 -> 3
1 -> 0
2 -> 1,6
3 -> 2
4 -> 2
5 -> 4
6 -> 5,8
7 -> 9
8 -> 7
9 -> 6
Sample Output:
 6->8->7->9->6->5->4->2->1->0->3->2->6
"""


graph = dict()
dnas = list()
filename = "dataset2_2.txt"
with open(filename) as fh:
	while True:
		seq = fh.readline().rstrip() 		
		if len(seq) == 0:
			break
		dnas.append(seq)
dnas2 = list()		
for i in dnas:
	dnas2.append(i.split())
for i in dnas2:
	list_1 = i[2].split(',')
	graph[i[0]] = set([])
	for j in list_1:
		graph[i[0]].add(j)
		if j not in graph:
			graph[j] = set()
def EulerianPath(graph):
	result = list()
	cycle = list()
	new_graph = graph
	unused_nodes = set(graph.keys())
	while len(unused_nodes) > 0:
		element = unused_nodes.pop()
		unused_nodes.add(element)
		cycle.append(element)
		element_1 = element
		while len(new_graph[element_1]) > 0:
			new_node = new_graph[element_1].pop()
			cycle.append(new_node)
			if len(new_graph[element_1]) == 0:
				new_graph.pop(element_1)
				unused_nodes.discard(element_1)
			element_1 = new_node
			if element_1 not in new_graph:
				while element_1 not in new_graph:
					cycle_re = list()
					if cycle[-1] != cycle[0]:
						cycle_re.append(cycle[-1])
						for n in range(len(cycle)-1):
							cycle_re.append(cycle[n])
					else:
						cycle_re = cycle[:-1]
					cycle = cycle_re
					element_1 = cycle[-1]
					if len(unused_nodes) == 0:
						result = cycle.append(cycle[0])
						break
			if len(unused_nodes) == 0:		
				break
			
		if element in new_graph and len(new_graph[element]) == 0:
			new_graph.pop(element)
			unused_nodes.discard(element)
	return cycle	

def compute_in_degrees(digraph):
    """
    Compute in_degrees
    """
    result = dict()
    for each in digraph:
        result[each] = 0
    for node in digraph:
        for node_2 in digraph:
            if node != node_2 and node in digraph[node_2]:
                result[node] += 1
    return result

in_degree = compute_in_degrees(graph)
for each in in_degree:
	out_degree = len(graph[each])
	if in_degree[each] > out_degree:
		tail = each
	elif in_degree[each] < out_degree:
		head = each

graph[tail].add(head)
result = EulerianPath(graph)
for pos in range(len(result)-1):
	if result[pos] == tail and result[pos+1] == head:
		result = result[pos+1:] + result[1:pos+1]
		break

print '->'.join(result)
