"""
Sample Input:
GAGG
CAGG
GGGG
GGGA
CAGG
AGGG
GGAG
Sample Output:
AGG -> GGG
CAG -> AGG,AGG
GAG -> AGG
GGA -> GAG
GGG -> GGA,GGG
"""
dnas = []
filename = "dataset5.txt"
with open(filename) as fh:
	while True:
		seq = fh.readline().rstrip() 		
		if len(seq) == 0:
			break
		dnas.append(seq)
mers_list = []
result = dict()
d = len(dnas[0]) - 1
for each in dnas:
	if each[0:d] not in mers_list:
		mers_list.append(each[0:d])
	#if each[1:] not in mers_list:
		#mers_list.append(each[1:])
mers_list.sort()
for i in mers_list:
	if i not in result:
		result[i] = list()
for m in result:
	for j in dnas:
		if m == j[:d]:
			result[m].append(j[1:])	
for k in mers_list:
	result[k].sort()
	str_ans = ""
	for n in range(len(result[k])):
		str_ans += result[k][n]
		if len(result[k]) - n > 1:
			str_ans += ","
	print k +" -> "+ str_ans
	 



