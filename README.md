# These scripts were written in Python as assignments for the Bioinformatics specialization (offered by UCSD on Coursera)

Short description of classes from Bioinformatics specialization:

1.  **Finding Hidden Messages in DNA** 
*Algorithms for finding special biological patterns in genetic sequences (replication origin, regulatory motifs)*
1. **Genome Sequencing**
*Graph theory algorithms for assembling short genomic sequences into full length genome*
1. **Comparing Genes, Proteins, and Genomes**
*Genomic sequence alignment (dynamic programming algorithms)*
1. **Molecular Evolution**
*Algorithms for constructing evolutionary trees*
1. **Genomic Data Science and Clustering**
*Clustering algorithms for population genetics*