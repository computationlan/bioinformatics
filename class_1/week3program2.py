"""
        Sample Input:
     3
     AAATTGACGCAT
     GACGACCACGTT
     CGTCAGCGCCTG
     GCTGAGCACCGG
     AGTTCGGGACAG
Sample Output:
     GAC"""

def HammingDistance(gen1, gen2):
	result = 0
	for i in range(len(gen1)):
		if gen1[i] == gen2[i]:
			pass
		else:
			result += 1
	return result

def gen_seqs(num):
	bases = "ACGT"
	result = []
	result1 = []
	for g in range(num):
		if len(result) == 0:
			for i in bases:
				result1.append(i)
		else:
			for j in result:
				for i in bases:
					result1.append(j+i)
		result = list(result1)
		result1 = []
	return result		

dna = ["AAATTGACGCAT", "GACGACCACGTT", "CGTCAGCGCCTG", "GCTGAGCACCGG", "AGTTCGGGACAG"] 
length = 3
distance = 2000

patterns = gen_seqs(length)
result = dict.fromkeys(patterns, 0)
median_pattern = ""
for dna_seq in dna:
	for pattern in patterns:
		dist_h = 2000
		for i in range(len(dna_seq)-length+1):
			fragment = dna_seq[i:i+length]
			comparison_n = HammingDistance(pattern, fragment)
			if comparison_n < dist_h:
				dist_h = comparison_n
		result[pattern] += dist_h
for m in result.keys():
	if result[m] < distance:
		distance = result[m]
		median_pattern = m			
print result
print median_pattern
	
