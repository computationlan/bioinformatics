#ACGTTGCATGTCGCATGATGCATGAGAGCT
#     k = 4 d = 1
# GATG ATGC ATGT
gen = "ACGTTGCATGTCGCATGATGCATGAGAGCT" 
pattern_k = 4
mismatch = 1
def num_to_str(n, k):
    alph = "ACGT"
    result = []
    while n>=4:
        result.append(n%4)
        n = n/4
    result.append(n)
    br = k - len(result)
    final = alph[0]*br
    for i in range(len(result)):
        final = final + alph[result.pop()]
    return final
input_list = [gen[i:i+pattern_k] for i in range(len(gen)-pattern_k+1)]
dict_count = dict()
for j in range(4**pattern_k):
    dict_count[num_to_str(j, pattern_k)] = 0
for pattern in dict_count.keys():
	for j in range(len(input_list)):
		count = 0
		for k in range(pattern_k):
			if input_list[j][k] == pattern[k]:
				pass
			else:
				count += 1
		if count <= mismatch:
			dict_count[pattern] += 1        
max = 0
for h in dict_count.keys():
	if dict_count[h] >= max:
		max = dict_count[h]
for h in dict_count.keys():
	if dict_count[h] == max:
		print h,