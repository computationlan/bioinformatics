#Approximate Pattern Matching Problem
#ATTCTGGA
#CGCCCGAATCCAGAACGCATTCCCATATTTCGGGACCACTGGCCTCCACGGTACGGACGTCAATCAAAT
#3
#Sample Output:
#6 7 26 27

gen = "CGCCCGAATCCAGAACGCATTCCCATATTTCGGGACCACTGGCCTCCACGGTACGGACGTCAATCAAAT"
pattern = "ATTCTGGA"
mismatch = 3

result = []
input_list = [gen[i:i+len(pattern)] for i in range(len(gen)-len(pattern)+1)]
for j in range(len(input_list)):
	count = 0
	for k in range(len(pattern)):
		if input_list[j][k] == pattern[k]:
			pass
		else:
			count += 1
	if count <= mismatch:
		print j,