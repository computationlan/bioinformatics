pattern = "AAA"
strings = ["TTACCTTAAC", "GATATCTGTC", "ACGGCGTTCG", "CCCTAAAGAG", "CGTCAGAGGT"]
result = 0
distance = 0
for j in strings:
	hamming = 2000
	for g in range(len(j)-len(pattern)+1):
		hamming_d = 0
		for i in range(len(pattern)):
			if pattern[i] == j[g:(g+len(pattern))][i]:
				pass
			else:
				hamming_d += 1
		if hamming_d < hamming:
			hamming = hamming_d
	distance = distance + hamming
print distance