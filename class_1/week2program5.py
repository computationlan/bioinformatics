gen = "TACGCATTACAAAGCACA"
pattern = "AA"
mismatch = 1
result = 0
input_list = [gen[i:i+len(pattern)] for i in range(len(gen)-len(pattern)+1)]
for j in range(len(input_list)):
	count = 0
	for k in range(len(pattern)):
		if input_list[j][k] == pattern[k]:
			pass
		else:
			count += 1
	if count <= mismatch:
		result += 1
print result