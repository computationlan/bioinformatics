import random

dnas = ['GGCGTTCAGGCA', 'AAGAATCAGTCA', 'CAAGGAGTTCGC', 'CACGTCAATCAC', 'CAATAATATTCG']
k = 3
t = len(dnas)

def get_kmer(str):
	pos = random.randrange(0, (len(dnas[0])-k+1))
	return str[pos:pos+k]

def prob_matrix(lst1):
	matrix_A = [] 
	matrix_C = []
	matrix_G = []
	matrix_T = []
	for j in range(k):
		sum_A = 1
		sum_C = 1
		sum_G = 1
		sum_T = 1
		for str in lst1:
			if str[j] == "A":
				sum_A += 1
			if str[j] == "C":
				sum_C += 1
			if str[j] == "G":
				sum_G += 1
			if str[j] == "T":
				sum_T += 1		
		matrix_A.append(float(sum_A)/float(len(lst1)+4))  
		matrix_C.append(float(sum_C)/float(len(lst1)+4)) 
		matrix_G.append(float(sum_G)/float(len(lst1)+4))
		matrix_T.append(float(sum_T)/float(len(lst1)+4))
	return [matrix_A, matrix_C, matrix_G, matrix_T]

def probability(str_in, prob_table):
	prob_s = 1
	for i in range(len(str_in)):
		if str_in[i] == "A":
			prob_s = prob_s*prob_table[0][i]
		elif str_in[i] == "C":
			prob_s = prob_s*prob_table[1][i]
		elif str_in[i] == "G":
			prob_s = prob_s*prob_table[2][i]
		elif str_in[i] == "T":
			prob_s = prob_s*prob_table[3][i]	
	return prob_s	

def most_probable_kmer(dna, prob_table):
	best_kmer = dna[0:k]
	best_p = probability(best_kmer, prob_table)
	for i in range(1, len(dna)-k+1):
		kmer = dna[i:i+k]
		p = probability(kmer, prob_table)
		if p > best_p:
			best_p = p
			best_kmer = kmer
	return best_kmer

def score(lst1):
	result = 0
	for j in range(k):
		sum_A = 0
		sum_C = 0
		sum_G = 0
		sum_T = 0
		for str in lst1:
			if str[j] == "A":
				sum_A += 1
			if str[j] == "C":
				sum_C += 1
			if str[j] == "G":
				sum_G += 1
			if str[j] == "T":
				sum_T += 1		
		sorted_counts = sorted([sum_A, sum_C, sum_G, sum_T])
		result += sum(sorted_counts[0:3])
	return result

def randomized_motif_search(dnas, k, t):
	motifs = [get_kmer(dna) for dna in dnas]
	best_motifs = list(motifs)
	best_score = score(best_motifs)
	while 1 > 0:
		profile = prob_matrix(motifs)
		motifs = list()
		for stri in dnas:
			new_motif = most_probable_kmer(stri, profile)
			motifs.append(new_motif)	
		curr_score = score(motifs)
		if curr_score < best_score:
			best_motifs = motifs
			best_score = curr_score
		else:
			return (best_motifs, best_score)

final_motifs = []
final_score = 20000			
for z in range(1000):
	current_motifs, current_score = randomized_motif_search(dnas, k, t)
	if final_score > current_score:
		final_score = current_score
		final_motifs = current_motifs
			
for motif in final_motifs:
	print motif