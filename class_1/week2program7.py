#ACG
#1
#Sample Output: CCG TCG GCG AAG ATG AGG ACA ACC ACT ACG
bases = "ACGT"
pattern = "AGTCTTCCCGG" 
input_list = [pattern]
distance = 2
result = []
while distance > 0:
	for each in input_list:
		for i in range(len(each)):
			for j in bases:
				seq = each[:i]+j+each[i+1:]
				if seq not in result:
					result.append(seq)
	input_list += result
	distance -= 1

for k in result:
	print k,