"""Sample Input:
     3 1
     ATTTGGC
     TGCCTTA
     CGGTATC
     GAAAATT
Sample Output:
     ATA ATT GTT TTT"""

bases = "ACGT"
pattern = ["AAATTGACGCAT", "GACGACCACGTT", "CGTCAGCGCCTG", "GCTGAGCACCGG", "AGTTCGGGACAG"]
input_str = pattern[0]
length = 3
distance = 1
result = set()
input_list = [input_str[i:i+length] for i in range(len(input_str)-length+1)]
distance1 = distance
result1 = set()
while distance1 > 0:
	for each in input_list:
		for i in range(len(each)):
			for j in bases:
				seq = each[:i]+j+each[i+1:]
				result1.add(seq)
	input_list += result1
	distance1 -= 1
pattern_lists = []
for j in range(len(pattern)-1): 
	in_str = pattern[j+1] 
	pattern_lists.append([in_str[i:i+length] for i in range(len(in_str)-length+1)])
results = []	
for w in range(len(pattern_lists)):
	distance2 = distance
	result2 = set()
	while distance2 > 0:
		input_list = pattern_lists[w]
		for each in input_list:
			for i in range(len(each)):
				for j in bases:
					seq = each[:i]+j+each[i+1:]
					result2.add(seq)
		input_list += result2
		distance2 -= 1
	results.append(result2)
for t in result1:
	counter = 1
	for k in range(len(results)):
		if t in results[k]:
			counter += 1
	if counter == len(pattern):
		result.add(t)
for k in result:
	print k,
	