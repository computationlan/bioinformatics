def num_to_str(n, k):
    alph = 'ACGT'
    result = []
    while n>=4:
        result.append(n%4)
        n = n/4
    result.append(n)
    br = k - len(result)
    final = alph[0]*br
    for i in range(len(result)):
        final = final + alph[result.pop()]
    return final

input_str = "CTAACCTGTAGCCGTGGATAAACGTGTTATAATCTGGTTCTTAACCGCCAGGCGGTAAGATCCCTTCACTTCCTCGATAGTCGCGATAGCTGGAAGAGCCAAGAGATTGTAAAGGAAATATTATGTTGGAGTCAGGCGTCGTCCGCACACCCTTTGTGGTAGATGTTGTTACAAGCGGTTAATAATCCACCAGGCGGCGCCAGAATAGTAAATGCTGCGACGCTGTGAATACGTACCTCACGCACTGTGTATCTAAAGCCAAAGGAGACTCGTGGATAACGGACATGACATGACTAATTCCAAACGTTAGTCTGGTCCGAACTTCTATCTGCCATTTGTAAACCTAACCTGCTCCAGCGCCAGATGTCAGTAGTCTTAATGTATTAACTTATTAGAGTTACATGAGTCGCTTCAACGGTGTCAAAGCACCTATCTTCATACACCGCAAAAAAATTGAAGACCATAGCAGCGTCTAGGCCTCACTCCTACCATGGGTGACAATTGAACAACAATGATCTCGAGTATTGGTTCTCATCAGCGGTTCATACTAGACAGAAAATCGACCTAGTTAAGCTCAGATCGCCGAGAGGTAAGCAGGTATGCTATACTAGTTACACACATATCTGGCCTATCTCACCACAA"
input_k = 5
input_list = [input_str[i:i+input_k] for i in range(len(input_str)-input_k+1)]
bases = "ACGT" 
dict_count = dict()
for i in range(len(input_list)):
    if not input_list[i] in dict_count:
        dict_count[input_list[i]] = 0
    dict_count[input_list[i]] += 1
for j in range(4**input_k):
    if num_to_str(j, input_k) in dict_count:
        print dict_count[num_to_str(j, input_k)],
    else:
        print 0,