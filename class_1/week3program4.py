"""Sample Input:
     3 5
     GGCGTTCAGGCA
     AAGAATCAGTCA
     CAAGGAGTTCGC
     CACGTCAATCAC
     CAATAATATTCG

Sample Output:
     CAG
     CAG
     CAA
     CAA
     CAA"""

k_mer = 3
t_num = 5
input_str = ["GGCGTTCAGGCA", "AAGAATCAGTCA", "CAAGGAGTTCGC", "CACGTCAATCAC", "CAATAATATTCG"]
def prob_matrix(lst1):
	matrix_A = [] 
	matrix_C = []
	matrix_G = []
	matrix_T = []
	for j in range(k_mer):
		sum_A = 0
		sum_C = 0
		sum_G = 0
		sum_T = 0
		for i in range(len(lst1)):
			if lst1[i][j] == "A":
				sum_A += 1
				print lst1[i][j], sum_A
			if lst1[i][j] == "C":
				sum_C += 1
			if lst1[i][j] == "G":
				sum_G += 1
			if lst1[i][j] == "T":
				sum_T += 1		
		matrix_A.append(float(sum_A)/float(len(lst1)))  
		matrix_C.append(float(sum_C)/float(len(lst1))) 
		matrix_G.append(float(sum_G)/float(len(lst1)))
		matrix_T.append(float(sum_T)/float(len(lst1)))
	return [matrix_A, matrix_C, matrix_G, matrix_T]

prob_table = prob_matrix(input_str)
