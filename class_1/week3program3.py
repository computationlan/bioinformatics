"""Sample Input:
     ACCTGTTTATTGCCTAAGTTCCGAACAAACCCAATATAGCCCGAGGGCCT
     5
     0.2 0.2 0.3 0.2 0.3
     0.4 0.3 0.1 0.5 0.1
     0.3 0.3 0.5 0.2 0.4
     0.1 0.2 0.1 0.1 0.2

Sample Output:
     CCGAG"""

matrix_A = [0.2, 0.2, 0.3, 0.2, 0.3]
matrix_C = [0.4, 0.3, 0.1, 0.5, 0.1]
matrix_G = [0.3, 0.3, 0.5, 0.2, 0.4]
matrix_T = [0.1, 0.2, 0.1, 0.1, 0.2]
k_mer = 5
input_str = "ACCTGTTTATTGCCTAAGTTCCGAACAAACCCAATATAGCCCGAGGGCCT"
prob = 0
prob_mer = ""
def probability(str_in):
	prob_s = 1
	for i in range(len(str_in)):
		if str_in[i] == "A":
			prob_s = prob_s*matrix_A[i]
		elif str_in[i] == "C":
			prob_s = prob_s*matrix_C[i]
		elif str_in[i] == "G":
			prob_s = prob_s*matrix_G[i]
		elif str_in[i] == "T":
			prob_s = prob_s*matrix_T[i]	
	return prob_s	
result = ""
for i in range(len(input_str)-k_mer+1):
	calc = probability(input_str[i:i+k_mer])
	if calc > prob:
		prob = calc
		result = input_str[i:i+k_mer]
	
print result