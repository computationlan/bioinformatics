dnas = ['GGCGTTCAGGCA', 'AAGAATCAGTCA', 'CAAGGAGTTCGC', 'CACGTCAATCAC', 'CAATAATATTCG']
k = 3
t = len(dnas)

def get_kmer(str, pos):
	return str[pos:pos+k]

def prob_matrix(lst1):
	matrix_A = [] 
	matrix_C = []
	matrix_G = []
	matrix_T = []
	for j in range(k):
		sum_A = 0
		sum_C = 0
		sum_G = 0
		sum_T = 0
		for str in lst1:
			if str[j] == "A":
				sum_A += 1
			if str[j] == "C":
				sum_C += 1
			if str[j] == "G":
				sum_G += 1
			if str[j] == "T":
				sum_T += 1		
		matrix_A.append(float(sum_A)/float(len(lst1)))  
		matrix_C.append(float(sum_C)/float(len(lst1))) 
		matrix_G.append(float(sum_G)/float(len(lst1)))
		matrix_T.append(float(sum_T)/float(len(lst1)))
	return [matrix_A, matrix_C, matrix_G, matrix_T]

def probability(str_in, prob_table):
	prob_s = 1
	for i in range(len(str_in)):
		if str_in[i] == "A":
			prob_s = prob_s*prob_table[0][i]
		elif str_in[i] == "C":
			prob_s = prob_s*prob_table[1][i]
		elif str_in[i] == "G":
			prob_s = prob_s*prob_table[2][i]
		elif str_in[i] == "T":
			prob_s = prob_s*prob_table[3][i]	
	return prob_s	

def most_probable_kmer(dna, prob_table):
	best_kmer = get_kmer(dna, 0)
	best_p = probability(best_kmer, prob_table)
	for i in range(1, len(dna)-k+1):
		kmer = get_kmer(dna, i)
		p = probability(kmer, prob_table)
		if p > best_p:
			best_p = p
			best_kmer = kmer
	return best_kmer

def score(lst1):
	result = 0
	for j in range(k):
		sum_A = 0
		sum_C = 0
		sum_G = 0
		sum_T = 0
		for str in lst1:
			if str[j] == "A":
				sum_A += 1
			if str[j] == "C":
				sum_C += 1
			if str[j] == "G":
				sum_G += 1
			if str[j] == "T":
				sum_T += 1		
		sorted_counts = sorted([sum_A, sum_C, sum_G, sum_T])
		result += sum(sorted_counts[0:3])
	return result

best_motifs = [get_kmer(dna, 0) for dna in dnas]
best_score = score(best_motifs)
for first_pos in range(len(dnas[0])-k+1):
	motifs = [ get_kmer(dnas[0], first_pos) ]
	for i in range(1, t):
		profile = prob_matrix(motifs)
		new_motif = most_probable_kmer(dnas[i], profile)
		motifs.append(new_motif)
	curr_score = score(motifs)
	if curr_score < best_score:
		best_motifs = motifs
		best_score = curr_score
for motif in best_motifs:
	print motif